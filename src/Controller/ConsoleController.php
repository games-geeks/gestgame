<?php

namespace App\Controller;

/**
 * \namespace App\Controller
 * \file          ConsoleController.php
 * \author    Denny
 * \version   1.0
 * \date       24 décembre 2020
 * \brief       Définit les actions de la classe Console.
 *
 * \details    Cette classe permet de gérer les objets de la classe Console.
 */


use App\Entity\Console;
use App\Form\ConsoleType;
use App\Repository\ConsoleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

#[Security("is_granted('GAME_MANAGE')")]
class ConsoleController extends AbstractController
{
    /**
     * @return Response
     */
    #[Route(path: '/console', name: 'console')]
    public function index(ConsoleRepository $repo): Response
    {
        $consoles = $repo->findBy([], ['longName' => 'ASC']);

        return $this->render('console/index.html.twig', [
            'controller_name' => 'ConsoleController',
            'consoles' => $consoles,
        ]);
    }

    #[Route(path: '/console/create', name: 'create_console', methods: ['GET', 'POST'])]
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        $console = new Console;

        $form = $this->createForm(ConsoleType::class, $console);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($console);
            $em->flush();
            //permet d'afficher un message
            $this->addFlash('success', 'Console successfully created!');
            return $this->redirectToRoute('console');
        }
        return $this->render('console/create.html.twig', [
            'controller_name' => 'consoleController',
            'monForm' => $form->createView(),
        ]);
    }

    /**
     * @return Response
     * \brief Fonction permettant d'afficher l'objet console
     *
     */
    #[Route(path: '/console/{id<[0-9]+>}', name: 'show_console', methods: ['GET'])]
    #[ParamConverter('console')]
    public function show(Console $console): Response
    {

        return $this->render('console/show.html.twig', [
            'controller_name' => 'consoleController',
            'console' => $console,
        ]);
    }

    #[Route(path: '/console/{id<[0-9]+>}', name: 'delete_console', methods: ['DELETE'])]
    #[ParamConverter('console')]
    public function delete(Request $request, EntityManagerInterface $em, Console $console): Response
    {
        if ($this->isCsrfTokenValid('console_deletion_' . $console->getId(), $request->request->get('csrf_token'))) {
            $em->remove($console);
            $em->flush();
            //permet d'afficher un message
            $this->addFlash('info', 'Console Supprimé!');
        }
        return $this->redirectToRoute('console');
    }
    #[Route(path: '/console/{id<[0-9]+>}/edit', name: 'edit_console', methods: ['GET', 'POST'])]
    #[ParamConverter('console')]
    public function edit(Request $request, EntityManagerInterface $em, Console $console): Response
    {
        $form = $this->createForm(ConsoleType::class, $console, [
            'method' => 'PUT',
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            //permet d'afficher un message
            $this->addFlash('success', 'Console Modifiée!');
            return $this->redirectToRoute('console');
        }
        return $this->render('console/edit.html.twig', [
            'controller_name' => 'consoleController',
            'console' => $console,
            'monForm' => $form->createView(),
        ]);
    }
}
