<?php

namespace App\Data;

use DateTime;
use IGDBUtils;

class ConsoleIDGB
{
    private $id;
    private $name;
    private $alternativeName;
    private $abbreviation;


    public function getID(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAlternativeName(): string
    {
        if (isset($this->alternativeName)) {
            return $this->alternativeName;
        } else {
            return "";
        }
    }
    public function getAbbreviation(): string
    {
        if (isset($this->abbreviation)) {
            return $this->abbreviation;;
        } else {
            return "";
        }
    }
    public function setID(int $id)
    {
        $this->id = $id;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }
    public function setAlternativeName(string $alternativeName)
    {
        $this->alternativeName = $alternativeName;
    }
    public function setAbbreviation(string $abbreviation)
    {
        $this->abbreviation = $abbreviation;
    }
    /*array:23 [▼
    +"id": 10    +"name": "Racing"
    +"id": 31    +"name": "Adventure"
    +"id": 5     +"name": "Shooter"
    +"id": 35    +"name": "Card & Board Game"
    +"id": 14    +"name": "Sport"


    +"id": 4     +"name": "Fighting"
    +"id": 7     +"name": "Music"
    +"id": 8     +"name": "Platform"
    +"id": 9     +"name": "Puzzle"
    +"id": 11    +"name": "Real Time Strategy (RTS)"
    +"id": 12    +"name": "Role-playing (RPG)"
    +"id": 13    +"name": "Simulator"
    +"id": 15    +"name": "Strategy"
    +"id": 16    +"name": "Turn-based strategy (TBS)"
    +"id": 24    +"name": "Tactical"
    +"id": 26    +"name": "Quiz/Trivia"
    +"id": 25    +"name": "Hack and slash/Beat 'em up"
    +"id": 30    +"name": "Pinball"
    +"id": 33    +"name": "Arcade"
    +"id": 34    +"name": "Visual Novel"
    +"id": 32    +"name": "Indie"
    +"id": 36    +"name": "MOBA"
    +"id": 2     +"name": "Point-and-click"

]*/
}
