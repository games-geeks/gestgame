<?php

namespace App\Controller\Admin;

use App\Entity\LangagePosition;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class LangagePositionCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return LangagePosition::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
