<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201128171724 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE game_users ADD consoles_id INT NOT NULL');
        $this->addSql('ALTER TABLE game_users ADD CONSTRAINT FK_26B0DC6687B3CA2D FOREIGN KEY (consoles_id) REFERENCES consoles (id)');
        $this->addSql('CREATE INDEX IDX_26B0DC6687B3CA2D ON game_users (consoles_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE game_users DROP FOREIGN KEY FK_26B0DC6687B3CA2D');
        $this->addSql('DROP INDEX IDX_26B0DC6687B3CA2D ON game_users');
        $this->addSql('ALTER TABLE game_users DROP consoles_id');
    }
}
