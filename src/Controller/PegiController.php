<?php

namespace App\Controller;

use App\Entity\Pegi;
use App\Form\PegiType;
use App\Repository\PegiRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

#[Security("is_granted('GAME_MANAGE')")]
class PegiController extends AbstractController
{
    #[Route(path: '/pegi', name: 'pegi')]
    public function index(PegiRepository $repo): Response
    {
        $pegis = $repo->findBy([], ['age' => 'ASC']);
        return $this->render('pegi/index.html.twig', [
            'pegis' => $pegis,
        ]);
    }

    #[Route(path: '/pegi/create', name: 'create_pegi', methods: ['GET', 'POST'])]
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        $pegi = new Pegi;

        $form = $this->createForm(PegiType::class, $pegi);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($pegi);
            $em->flush();
            //permet d'afficher un message
            $this->addFlash('success', 'PEGI successfully created!');
            return $this->redirectToRoute('pegi');
        }
        return $this->render('pegi/create.html.twig', [
            'monForm' => $form->createView(),
        ]);
    }

    #[Route(path: '/pegi/{id<[0-9]+>}', name: 'show_pegi', methods: ['GET'])]
    #[ParamConverter('pegi')]
    public function show(Pegi $pegi): Response
    {

        return $this->render('pegi/show.html.twig', [
            'pegi' => $pegi,
        ]);
    }


    #[Route(path: '/pegi/{id<[0-9]+>}', name: 'delete_pegi', methods: ['DELETE'])]
    #[ParamConverter('pegi')]
    public function delete(Request $request, EntityManagerInterface $em, Pegi $pegi): Response
    {
        if ($this->isCsrfTokenValid('pegi_deletion_' . $pegi->getId(), $request->request->get('csrf_token'))) {
            $em->remove($pegi);
            $em->flush();
            //permet d'afficher un message
            $this->addFlash('info', 'PEGI Supprimé!');
        }
        return $this->redirectToRoute('pegi');
    }
    #[Route(path: '/pegi/{id<[0-9]+>}/edit', name: 'edit_pegi', methods: ['GET', 'PUT'])]
    #[ParamConverter('pegi')]
    public function edit(Request $request, EntityManagerInterface $em, Pegi $pegi): Response
    {
        $form = $this->createForm(PegiType::class, $pegi, [
            'method' => 'PUT',
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            //permet d'afficher un message
            $this->addFlash('success', 'PEGI Modifié!');
            return $this->redirectToRoute('pegi');
        }
        return $this->render('pegi/edit.html.twig', [
            'pegi' => $pegi,
            'monForm' => $form->createView(),
        ]);
    }
}
