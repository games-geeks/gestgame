<?php

namespace App\Entity;

use Tzsk\Collage\MakeCollage;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\SerieRepository;
use App\Entity\Traits\Timestampable;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Table(name: 'series')]
#[ORM\Entity(repositoryClass: SerieRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Serie
{

    use Timestampable;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255, unique: true)]
    #[Assert\NotBlank(message: 'Le nom ne peut être vide')]
    private $name;

    #[ORM\OneToMany(targetEntity: Game::class, mappedBy: 'serie')]
    private $games;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $cover;

    public function __construct()
    {
        $this->games = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Game[]
     */
    public function getGames(): Collection
    {
        return $this->games;
    }

    public function addGame(Game $game): self
    {
        if (!$this->games->contains($game)) {
            $this->games[] = $game;
            $game->setSerie($this);
            $this->makeCollage();
        }

        return $this;
    }

    public function removeGame(Game $game): self
    {
        if ($this->games->removeElement($game)) {
            // set the owning side to null (unless already changed)
            if ($game->getSerie() === $this) {
                $game->setSerie(null);
            }
        }

        return $this;
    }

    public function getCover(): ?string
    {
        return $this->cover;
    }

    public function setCover(?string $cover): self
    {
        $this->cover = $cover;
        return $this;
    }

    /**
     * Pour une série renvoie un collage d'image
     * @return string Chemin de l'image
     */
    public function makeCollage(): string
    {
        $collage = new MakeCollage(); // Default: 'gd'
        $images =  array();
        $nbGame = $this->getGames()->count();
        if ($nbGame > 4) {
            $nbGame = 4;
        }
        foreach ($this->games as $game) {
            if (count($images) <= 3) {
                array_push($images, 'uploads/covers/' . $game->getCover());
            }
        }
        //dd($images);
        $image = $collage->make(800, 800)->from($images);
        $image->save($this->getCover(), 100, 'jpg');
        return '';
    }
    /**
     * Permet de supprimer l'image en cas de suppression de la série
     */
    public function removeFile()
    {
        unlink('uploads/covers/serie-' . $this->getCover() . '.jpg');
    }

    /**
     * Permet de modifier le nom de l'image en cas de modification de la série
     */
    public function renameFile(string $sOld, string $sNew)
    {
        rename($sOld, $sNew);
    }

    public function __toString()
    {
        return $this->getName();
    }
}
