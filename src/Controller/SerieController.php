<?php

namespace App\Controller;

use App\Entity\Serie;
use App\Form\SerieType;
use Cocur\Slugify\Slugify;
use App\Repository\GameRepository;
use App\Repository\SerieRepository;
use App\Service\PhotoCollageService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[IsGranted('GAME_MANAGE')]
class SerieController extends AbstractController
{

    public function __construct(
        private CacheManager $imagineCacheManager,
        private PhotoCollageService $photoCollageService
    ) {}
    #[Route(path: '/serie', name: 'serie')]
    public function index(SerieRepository $repo): Response
    {
        $series = $repo->findBy([], ['name' => 'ASC']);
        return $this->render('serie/index.html.twig', [
            'series' => $series,
        ]);
    }

    #[Route(path: '/serie/create', name: 'create_serie', methods: ['GET', 'POST'])]
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        $serie = new Serie;

        $form = $this->createForm(SerieType::class, $serie);

        $form->handleRequest($request);
        $slug = new Slugify();
        if ($form->isSubmitted() && $form->isValid()) {
            $serie->setCover('uploads/covers/serie-' .  $slug->slugify($serie->getName()) . '.jpg');
            $em->persist($serie);
            $em->flush();
            //permet d'afficher un message
            $this->addFlash('success', 'Serie successfully created!');
            return $this->redirectToRoute('serie');
        }
        return $this->render('serie/create.html.twig', [
            'monForm' => $form->createView(),
        ]);
    }

    #[Route(path: '/serie/{id<[0-9]+>}', name: 'show_serie', methods: ['GET'])]
    public function show(Serie $serie): Response
    {

        return $this->render('serie/show.html.twig', [
            'serie' => $serie,
        ]);
    }


    #[Route(path: '/serie/{id<[0-9]+>}', name: 'delete_serie', methods: ['DELETE'])]
    public function delete(Request $request, EntityManagerInterface $em, Serie $serie): Response
    {
        if ($this->isCsrfTokenValid('serie_deletion_' . $serie->getId(), $request->request->get('csrf_token'))) {
            $serie->removeFile();
            $em->remove($serie);
            $em->flush();
            //permet d'afficher un message
            $this->addFlash('info', 'Serie Supprimée!');
        }
        return $this->redirectToRoute('serie');
    }
    #[Route(path: '/serie/{id<[0-9]+>}/edit', name: 'edit_serie', methods: ['GET', 'PUT', 'POST'])]
    public function edit(Request $request, EntityManagerInterface $em, Serie $serie): Response
    {
        $form = $this->createForm(SerieType::class, $serie, [
            'method' => 'POST',
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $slug = new Slugify();
            if ($serie->getCover()) {
                $serie->renameFile($serie->getCover(), 'uploads/covers/serie-' . $slug->slugify($serie->getName())  . '.jpg');
            }
            $serie->setCover('uploads/covers/serie-' .  $slug->slugify($serie->getName()) . '.jpg');
            $em->flush();
            //permet d'afficher un message
            $this->addFlash('success', 'Serie Modifié!e');
            return $this->redirectToRoute('serie');
        }
        return $this->render('serie/edit.html.twig', [
            'serie' => $serie,
            'monForm' => $form->createView(),
        ]);
    }

    #[Route(path: '/serie/cover', name: 'cover_serie', methods: ['GET', 'PUT', 'POST'])]
    public function cover(SerieRepository $repo): Response
    {
        $series = $repo->findBy([], ['name' => 'ASC']);
        // foreach ($series as $serie) {
        //     $serie->makeCollage();
        // }
        return $this->render('serie/cover.html.twig', [
            'series' => $series,
        ]);
    }
    #[Route(path: '/serie/cover_cache', name: 'cover_cache', methods: ['GET', 'PUT', 'POST'])]
    public function clearCache(SerieRepository $repo, UrlGeneratorInterface $router): Response
    {
        $baseUrl = $router->generate('app_home', [], UrlGeneratorInterface::ABSOLUTE_URL);
        $series = $repo->findBy([], ['name' => 'ASC']);
        $this->imagineCacheManager->remove(null, 'squared_thumbnail_medium_serie');
        foreach ($series as $serie) {
            $images = array();
            $nbGame = $serie->getGames()->count();
            if ($nbGame > 4) {
                $nbGame = 4;
            }
            foreach ($serie->getGames() as $game) {
                if (count($images) <= 3) {
                    // dd($game->getCover());
                    array_push($images, $baseUrl . $game->getCover());
                }
            }
            // dd($images);
            $outputFile = 'uploads/covers/serie-' . $serie->getCover();
            $collagePath = $this->photoCollageService->createCollage($images, 2, 2, $outputFile, 'jpg', 5);
            dd($collagePath);
        }
        return $this->render('serie/cover.html.twig', [
            'series' => $series,
        ]);
    }
}
