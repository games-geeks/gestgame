<?php

namespace App\Controller;

use App\Entity\Constructeur;
use App\Form\ConstructeurType;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\ConstructeurRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

#[Security("is_granted('GAME_MANAGE')")]
class ConstructeurController extends AbstractController
{
    #[Route(path: '/constructeur', name: 'constructeur')]
    public function index(ConstructeurRepository $repo): Response
    {
        $constructeurs = $repo->findBy([], ['name' => 'ASC']);

        return $this->render('constructeur/index.html.twig', [
            'controller_name' => 'ConstructeurController',
            'constructeurs' => $constructeurs,
        ]);
    }


    #[Route(path: '/constructeur/create', name: 'create_constructeur', methods: ['GET', 'POST'])]
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        $constructeur = new Constructeur;

        $form = $this->createForm(ConstructeurType::class, $constructeur);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($constructeur);
            $em->flush();
            //permet d'afficher un message
            $this->addFlash('success', 'Constructeur successfully created!');
            return $this->redirectToRoute('constructeur');
        }
        return $this->render('constructeur/create.html.twig', [
            'controller_name' => 'constructeurController',
            'monForm' => $form->createView(),
        ]);
    }

    #[Route(path: '/constructeur/{id<[0-9]+>}', name: 'show_constructeur', methods: ['GET'])]
    #[ParamConverter('constructeur')]
    public function show(Constructeur $constructeur): Response
    {

        return $this->render('constructeur/show.html.twig', [
            'controller_name' => 'constructeurController',
            'constructeur' => $constructeur,
        ]);
    }


    #[Route(path: '/constructeur/{id<[0-9]+>}', name: 'delete_constructeur', methods: ['DELETE'])]
    #[ParamConverter('constructeur')]
    public function delete(Request $request, EntityManagerInterface $em, Constructeur $constructeur): Response
    {
        if ($this->isCsrfTokenValid(
            'constructeur_deletion_' . $constructeur->getId(),
            $request->request->get('csrf_token')
        )) {
            $em->remove($constructeur);
            $em->flush();
            //permet d'afficher un message
            $this->addFlash('info', 'Constructeur Supprimé!');
        }
        return $this->redirectToRoute('constructeur');
    }
    #[Route(path: '/constructeur/{id<[0-9]+>}/edit', name: 'edit_constructeur', methods: ['GET', 'POST'])]
    #[ParamConverter('constructeur')]
    public function edit(Request $request, EntityManagerInterface $em, Constructeur $constructeur): Response
    {
        $form = $this->createForm(ConstructeurType::class, $constructeur, [
            'method' => 'PUT',
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            //permet d'afficher un message
            $this->addFlash('success', 'Constructeur Modifié!');
            return $this->redirectToRoute('constructeur');
        }
        return $this->render('constructeur/edit.html.twig', [
            'controller_name' => 'constructeurController',
            'constructeur' => $constructeur,
            'monForm' => $form->createView(),
        ]);
    }
}
