<?php

namespace App\Data;

use DateTime;
use IGDBUtils;

class GameIGDB
{
    private $id;
    private $name;
    private $cover;
    private $storyline;
    private $slug;
    private $releaseDate;
    private $category;
    private $console;




    public function getID(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCover(): string
    {
        if (isset($this->cover)) {
            return $this->cover;
        } else {
            return "";
        }
    }

    public function getStoryline(): string
    {
        if (isset($this->storyline)) {
            return $this->storyline;
        } else {
            return "";
        }
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getReleaseDate()
    {
        return $this->releaseDate;
    }

    public function setID(int $id)
    {
        $this->id = $id;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }
    public function setCover(string $cover)
    {
        $this->cover = IGDBUtils::image_url($cover, 'cover_big_2x');
    }
    public function setStoryline(string $storyline)
    {
        $this->storyline = $storyline;
    }

    public function setSlug(string $slug)
    {
        $this->slug = $slug;
    }

    public function setReleaseDate(string $releaseDate)
    {
        $this->releaseDate = date("Y", $releaseDate);;
    }

    /**
     * Get the value of category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set the value of category
     *
     * @return  self
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get the value of console
     */
    public function getConsole()
    {
        return $this->console;
    }

    /**
     * Set the value of console
     *
     * @return  self
     */
    public function setConsole($console)
    {
        $this->console = $console;

        return $this;
    }
}
// name	value
// main_game	0
// dlc_addon	1
// expansion	2
// bundle	3
// standalone_expansion	4
// mod	5
// episode	6
// season	7
// remake	8
// remaster	9
// expanded_game	10
// port	11
// fork	12
// pack	13
// update	14