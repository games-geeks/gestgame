<?php

namespace App\Form;

use App\Entity\Console;
use App\Entity\Constructeur;
use App\Form\ConstructeurType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class ConsoleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('longName', TextType::class, ['label' => "Nom long de la console", 'attr' => ['autofocus' => true]])
            ->add('shortName', TextType::class, ['label' => "Nom court de la console"])
            ->add('idIgdb', IntegerType::class, ['label' => 'ID de la console sur IGDB'])
            ->add('constructeur', EntityType::class, [
                'class' => Constructeur::class,
                'choice_label' => 'name',
                'multiple' => false,
                'placeholder' => 'Choose an option',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Console::class,
        ]);
    }
}
