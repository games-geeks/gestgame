<?php

namespace App\Controller\Admin;

use App\Entity\Prix;
use App\Entity\Genre;
use App\Entity\Editeur;
use App\Entity\Enseigne;
use App\Entity\Developpeur;
use App\Entity\Game;
use App\Entity\Langage;
use App\Entity\LangagePosition;
use App\Entity\Screenshot;
use App\Entity\StatutJeu;
use App\Entity\Video;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

class DashboardController extends AbstractDashboardController
{
    #[Route(path: '/admin', name: 'gestion')]
    public function index(): Response
    {
        return $this->render('admin/dashbard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Gestgame');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::section('Jeu');
        yield MenuItem::linkToCrud('Développeur', 'fas fa-laptop-code', Developpeur::class);
        yield MenuItem::linkToCrud('Editeur', 'fa fa-address-card', Editeur::class);
        yield MenuItem::linkToCrud('Genre', 'fa fa-cog', Genre::class);
        yield MenuItem::section('Media');
        yield MenuItem::linkToCrud('Video', 'fa fa-video', Video::class);
        yield MenuItem::linkToCrud('Screenshot', 'far fa-image', Screenshot::class);
        yield MenuItem::section('Support');
        yield MenuItem::linkToCrud('Langue', 'fa-solid fa-language', Langage::class);
        yield MenuItem::linkToCrud('Langue Type', 'fa-solid fa-closed-captioning', LangagePosition::class);
        yield MenuItem::linkToCrud('Statut', 'fas fa-gamepad', StatutJeu::class);
        yield MenuItem::linkToCrud('Enseigne', 'fas fa-store', Enseigne::class);
        yield MenuItem::linkToCrud('Jeu', 'fas fa-gamepad', Game::class);
        yield MenuItem::linkToCrud('Prix', 'fas fa-euro-sign', Prix::class);


        // yield MenuItem::linkToCrud('The Label', 'fas fa-list', EntityClass::class);
    }
}
