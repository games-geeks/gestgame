<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201125210126 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout des relations Editeur/Game ';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE game_editeur (game_id INT NOT NULL, editeur_id INT NOT NULL, INDEX IDX_906538C0E48FD905 (game_id), INDEX IDX_906538C03375BD21 (editeur_id), PRIMARY KEY(game_id, editeur_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE game_editeur ADD CONSTRAINT FK_906538C0E48FD905 FOREIGN KEY (game_id) REFERENCES games (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE game_editeur ADD CONSTRAINT FK_906538C03375BD21 FOREIGN KEY (editeur_id) REFERENCES editeurs (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE game_editeur');
    }
}
