<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231029123923 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'gestion des langues dans un jeu';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE game_langage (game_id INT NOT NULL, langage_id INT NOT NULL, INDEX IDX_59929509E48FD905 (game_id), INDEX IDX_59929509957BB53C (langage_id), PRIMARY KEY(game_id, langage_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE game_langage ADD CONSTRAINT FK_59929509E48FD905 FOREIGN KEY (game_id) REFERENCES games (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE game_langage ADD CONSTRAINT FK_59929509957BB53C FOREIGN KEY (langage_id) REFERENCES langage (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE game_langage DROP FOREIGN KEY FK_59929509E48FD905');
        $this->addSql('ALTER TABLE game_langage DROP FOREIGN KEY FK_59929509957BB53C');
        $this->addSql('DROP TABLE game_langage');
    }
}
