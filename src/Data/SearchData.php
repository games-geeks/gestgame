<?php

namespace App\Data;

use App\Entity\Pegi;
use App\Entity\Genre;
use App\Entity\Serie;
use App\Entity\Console;
use App\Entity\Editeur;
use App\Entity\StatutJeu;
use App\Entity\Developpeur;


class SearchData
{
    /**
     * @var int
     */
    private $page = 1;
    /**
     * @var string
     */
    public $qSearch = '';

    /**
     * @var Pegi[]
     */
    public $pegis = [];

    /**
     * @var Console[]
     */
    public $consoles = [];

    /**
     * @var Genre[]
     */
    public $genres = [];

    /**
     * @var Developpeur[]
     */
    public $developpeurs = [];

    /**
     * @var Editeur[]
     */
    public $editeurs = [];

    /**
     * @var Serie[]
     */
    public $series = [];

    /**
     * @var StatutJeu[]
     */
    public $statutJeux = [];

    /**
     * @var boolean
     */
    public $isDemat = false;

    /**
     * @var boolean
     */
    public $isWish = false;

    /**
     * @var boolean
     */
    public $isLike = false;

    public function getPage()
    {
        return $this->page;
    }
    public function setPage(int $page)
    {
        $this->page = $page;
    }

    /**
     * Set the value of developpeurs
     *
     * @param  Developpeur[]  $developpeurs
     *
     * @return  self
     */
    public function setDeveloppeurs(Developpeur $developpeurs)
    {
        $this->developpeurs = $developpeurs;

        return $this;
    }

    /**
     * Set the value of series
     *
     * @param  Serie[]  $series
     *
     * @return  self
     */
    public function setSeries(Serie $series)
    {
        $this->series = $series;

        return $this;
    }

    /**
     * Set the value of editeurs
     *
     * @param  Editeur[]  $editeurs
     *
     * @return  self
     */
    public function setEditeurs(Editeur $editeurs)
    {
        $this->editeurs = $editeurs;

        return $this;
    }

    /**
     * Set the value of genres
     *
     * @param  Genre[]  $genres
     *
     * @return  self
     */
    public function setGenres(Genre $genres)
    {
        $this->genres = $genres;

        return $this;
    }

    /**
     * Set the value of status
     *
     * @param  StatutJeu[]  $statutJeux
     *
     * @return  self
     */
    public function setStatut(StatutJeu $statuts)
    {

        $this->statutJeux = $statuts;

        return $this;
    }
}
