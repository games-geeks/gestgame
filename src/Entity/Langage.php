<?php

namespace App\Entity;

use App\Repository\LangageRepository;
use App\Entity\Traits\Timestampable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: LangageRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Langage
{

    use Timestampable;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $idIgbd;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $locale = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $nativeName = null;

    #[ORM\OneToMany(mappedBy: 'langue', targetEntity: GameLanguage::class)]
    private Collection $gameLanguages;

    public function __construct()
    {
        $this->gameLanguages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(?string $locale): static
    {
        $this->locale = $locale;

        return $this;
    }

    public function getNativeName(): ?string
    {
        return $this->nativeName;
    }

    public function setNativeName(?string $nativeName): static
    {
        $this->nativeName = $nativeName;

        return $this;
    }


    public function __toString()
    {
        return $this->getName();
    }

    public function getIdIgbd(): ?int
    {
        return $this->idIgbd;
    }

    public function setIdIgbd(?int $idIgdbd): self
    {
        $this->idIgbd = $idIgdbd;
        return $this;
    }

    /**
     * @return Collection<int, GameLanguage>
     */
    public function getGameLanguages(): Collection
    {
        return $this->gameLanguages;
    }

    public function addGameLanguage(GameLanguage $gameLanguage): static
    {
        if (!$this->gameLanguages->contains($gameLanguage)) {
            $this->gameLanguages->add($gameLanguage);
            $gameLanguage->setLangue($this);
        }

        return $this;
    }

    public function removeGameLanguage(GameLanguage $gameLanguage): static
    {
        if ($this->gameLanguages->removeElement($gameLanguage)) {
            // set the owning side to null (unless already changed)
            if ($gameLanguage->getLangue() === $this) {
                $gameLanguage->setLangue(null);
            }
        }

        return $this;
    }
}
