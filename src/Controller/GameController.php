<?php

namespace App\Controller;

use App\Entity\Game;
use App\Entity\Genre;
use App\Entity\Serie;
use App\Form\GameType;
use App\Entity\Editeur;
use App\Data\SearchData;
use Cocur\Slugify\Slugify;
use App\Entity\Developpeur;
use App\Form\SearchDataType;
use App\Form\SearchGameType;
use App\Repository\GameRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class GameController extends AbstractController
{


    #[Route('/', name: 'app_home', methods: ['GET'])]
    public function index(GameRepository $repo, Request $request): Response
    {
        //$games = $repo->findBy([], ['title' => 'ASC']);
        $data = new SearchData();
        $data->setPage((int) $request->get('page', 1));
        //Ajout de la recherche. A voir si on ne crée pas une page à part
        $form = $this->createForm(SearchDataType::class, $data);
        $form->handleRequest($request);
        $games = $repo->findSearch($data, $this->getParameter('app.affich'));


        return $this->render('game/index.html.twig', [
            'controller_name' => 'GameController',
            'games' => $games,
            'titre' => 'Bienvenue sur GestGame! ',
            'form' => $form->createView(),
        ]);
    }


    public function index_old(PaginatorInterface $paginator, GameRepository $repo, Request $request): Response
    {
        $games = $repo->findBy([], ['title' => 'ASC']);


        //Ajout de la recherche. A voir si on ne crée pas une page à part
        $form = $this->createForm(SearchGameType::class);

        $search = $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // On recherche les annonces correspondant aux mots clés // Voir pourquoi $request->request ne marhce pas
            $games = $repo->search($search->get('mots')->getData());
            //$search->get('categorie')->getData()
        }

        $articles = $paginator->paginate(
            // Doctrine Query, not results
            $games,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            52
        );

        $articles->setCustomParameters([
            'align' => 'center', # center|right (for template: twitter_bootstrap_v4_pagination)
            'size' => 'small', # small|large (for template: twitter_bootstrap_v4_pagination)
            'style' => 'bottom',
            'span_class' => 'whatever',
        ]);

        return $this->render('game/index.html.twig', [
            'controller_name' => 'GameController',
            'games' => $articles,
            'titre' => 'Bienvenue sur GestGame! ',
            'monForm' => $form->createView(),
        ]);
    }


    #[Route(path: '/game/create', name: 'create_game', methods: ['GET', 'POST'])]
    #[IsGranted('GAME_MANAGE', message: 'No access! Get out!')]
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        if (!$this->getUser()) {
            $this->addFlash('error', 'Not Connected!');
            return $this->redirectToRoute('app_login');
        }

        $game = new Game;

        $form = $this->createForm(GameType::class, $game);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $slug = new Slugify();
            $game->setSlug($slug->slugify($game->getTitle()));
            $em->persist($game);
            $em->flush();
            if ($game->getSerie()) {
                $game->getSerie()->makeCollage();
            }
            $id = $game->getId();
            //permet d'afficher un message
            $this->addFlash('success', 'game successfully created!');
            return $this->redirectToRoute('show_game', ['id' => $id]);
        }
        return $this->render('game/create.html.twig', [
            'controller_name' => 'GameController',
            'monForm' => $form->createView(),
        ]);
    }


    #[Route('/game/{id<[0-9]+>}', name: 'show_game', methods: ['GET'])]
    public function show(Game $game): Response
    {

        return $this->render('game/show.html.twig', [
            'controller_name' => 'GameController',
            'game' => $game,
        ]);
    }
    #[Route(path: '/game/{id<[0-9]+>}', name: 'delete_game', methods: ['DELETE'])]
    #[IsGranted('GAME_MANAGE', message: 'No access! Get out!')]
    // #[ParamConverter('game')]
    public function delete(Request $request, EntityManagerInterface $em, Game $game): Response
    {
        if ($this->isCsrfTokenValid('game_deletion_' . $game->getId(), $request->request->get('csrf_token'))) {
            $em->remove($game);
            $em->flush();

            //permet d'afficher un message
            $this->addFlash('info', 'Jeu Supprimé!');
        }
        return $this->redirectToRoute('app_home');
    }

    #[Route(path: '/game/{id<[0-9]+>}/edit', name: 'edit_game', methods: ['GET', 'POST', 'PUT'])]
    #[IsGranted('GAME_MANAGE', message: 'No access! Get out!')]
    // #[ParamConverter('game')]
    public function edit(Request $request, EntityManagerInterface $em, Game $game): Response
    {
        $form = $this->createForm(GameType::class, $game, [
            'method' => 'POST',
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $filesystem = new Filesystem();
            $em->flush();
            // if ($game->getSerie()) {
            //     //                $resolvedPath = $this->imagineCacheManager->getBrowserPath($game->getSerie()->getCover(), 'squared_thumbnail_medium_serie');
            //     //$this->imagineCacheManager->remove(null, 'squared_thumbnail_medium_serie');
            //     //              $filesystem->remove($resolvedPath);
            //     //            $this->imagineCacheManager->resolve($resolvedPath, 'squared_thumbnail_medium_serie');
            //     //dd($resolvedPath);
            //     $game->getSerie()->makeCollage();
            // }
            //permet d'afficher un message
            $this->addFlash('success', 'Jeu Modifié!');
            return $this->redirectToRoute('show_game', ['id' => $game->getId()]);
        }
        return $this->render('game/edit.html.twig', [
            'controller_name' => 'GameController',
            'game' => $game,
            'monForm' => $form->createView(),
        ]);
    }

    #[Route(path: '/game/serie/{id<[0-9]+>}', name: 'serie_game', methods: ['GET'])]
    // #[ParamConverter('serie')]
    public function getGameForSerie(

        GameRepository $repo,
        Request $request,
        Serie $serie
    ): Response {


        //    $games = $repo->findBy(['serie' => $serie], ['title' => 'ASC']);
        //$games = $repo->findBy([], ['title' => 'ASC']);
        $data = new SearchData();
        $data->setPage((int) $request->get('page', 1));
        $data->setSeries($serie);
        //Ajout de la recherche. A voir si on ne crée pas une page à part
        $form = $this->createForm(SearchDataType::class, $data);
        $form->handleRequest($request);
        $games = $repo->findSearch($data, $this->getParameter('app.affich'));


        return $this->render('game/index.html.twig', [
            'controller_name' => 'GameController',
            'games' => $games,
            'titre' => 'Les jeux pour une série! ',
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/game/developpeur/{id<[0-9]+>}', name: 'developpeur_game', methods: ['GET'])]
    // #[ParamConverter('developpeur')]
    public function getGameForDeveloppeur(
        GameRepository $repo,
        Request $request,
        Developpeur $developpeur
    ): Response {

        //$games = $repo->findBy([], ['title' => 'ASC']);
        $data = new SearchData();
        $data->setPage((int) $request->get('page', 1));
        $data->setDeveloppeurs($developpeur);
        //Ajout de la recherche. A voir si on ne crée pas une page à part
        $form = $this->createForm(SearchDataType::class, $data);
        $form->handleRequest($request);
        $games = $repo->findSearch($data, $this->getParameter('app.affich'));


        return $this->render('game/index.html.twig', [
            'controller_name' => 'GameController',
            'games' => $games,
            'titre' => 'Les jeux pour un développeur! ',
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/game/editeur/{id<[0-9]+>}', name: 'editeur_game', methods: ['GET'])]
    // #[ParamConverter('editeur')]
    public function getGameForEditeur(
        GameRepository $repo,
        Request $request,
        Editeur $editeur
    ): Response {



        //$games = $repo->findBy([], ['title' => 'ASC']);
        $data = new SearchData();
        $data->setPage((int) $request->get('page', 1));
        $data->setEditeurs($editeur);
        //Ajout de la recherche. A voir si on ne crée pas une page à part
        $form = $this->createForm(SearchDataType::class, $data);
        $form->handleRequest($request);
        $games = $repo->findSearch($data, $this->getParameter('app.affich'));


        return $this->render('game/index.html.twig', [
            'controller_name' => 'GameController',
            'games' => $games,
            'titre' => 'Les jeux pour un édtieur! ',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/game/genre/{id<[0-9]+>}", name="genre_game",methods={"GET"})
     * @ParamConverter("genre")
     * Inutile depuis le filtre
     */
    // public function getGameForGenre(
    //     GameRepository $repo,
    //     Request $request,
    //     Genre $genre
    // ): Response {
    //     //$games = $repo->findBy([], ['title' => 'ASC']);
    //     $data = new SearchData();
    //     $data->setPage((int) $request->get('page', 1));
    //     $data->setGenres($genre);
    //     //Ajout de la recherche. A voir si on ne crée pas une page à part
    //     $form = $this->createForm(SearchDataType::class, $data);
    //     $form->handleRequest($request);
    //     $games = $repo->findSearch($data, $this->getParameter('app.affich'));
    //     dd($games);
    //     return $this->render('game/index.html.twig', [
    //         'controller_name' => 'GameController',
    //         'games' => $games,
    //         'titre' => 'Les jeux pour un genre! ',
    //         'form' => $form->createView(),
    //     ]);
    // }

    #[Route(path: '/game/slug', name: 'outils_slug', methods: ['GET'])]
    public function generateSlug(GameRepository $repo, EntityManagerInterface $em)
    {
        $games = $repo->findBy(['slug' => null], ['title' => 'ASC']);
        $nb = 0;
        $slug = new Slugify();
        foreach ($games as $game) {
            $game->setSlug($slug->slugify($game->getTitle()));
            $em->flush();
            $nb++;
        }
        $this->addFlash('info', $nb . ' Slug générés !');
        return $this->redirectToRoute('app_home');
    }
}
