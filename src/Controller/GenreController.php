<?php

namespace App\Controller;

use IGDB;
use App\Entity\Genre;
use IGDBQueryBuilder;
use App\Form\GenreType;
use App\Repository\GenreRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

#[Security("is_granted('GAME_MANAGE')")]
class GenreController extends AbstractController
{
    #[Route(path: '/genre', name: 'genre')]
    public function index(GenreRepository $repo): Response
    {
        $genres = $repo->findBy([], ['name' => 'ASC']);

        return $this->render('genre/index.html.twig', [
            'controller_name' => 'GenreController',
            'genres' => $genres,
        ]);
    }


    #[Route(path: '/genre/create', name: 'create_genre', methods: ['GET', 'POST'])]
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        $genre = new Genre;

        $form = $this->createForm(GenreType::class, $genre);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($genre);
            $em->flush();
            //permet d'afficher un message
            $this->addFlash('success', 'Genre successfully created!');
            return $this->redirectToRoute('genre');
        }
        return $this->render('genre/create.html.twig', [
            'controller_name' => 'GenreController',
            'monForm' => $form->createView(),
        ]);
    }

    #[Route(path: '/genre/{id<[0-9]+>}', name: 'show_genre', methods: ['GET'])]
    #[ParamConverter('genre')]
    public function show(Genre $genre): Response
    {

        return $this->render('genre/show.html.twig', [
            'controller_name' => 'GenreController',
            'genre' => $genre,
        ]);
    }


    #[Route(path: '/genre/{id<[0-9]+>}', name: 'delete_genre', methods: ['DELETE'])]
    #[ParamConverter('genre')]
    public function delete(Request $request, EntityManagerInterface $em, Genre $genre): Response
    {
        if ($this->isCsrfTokenValid('genre_deletion_' . $genre->getId(), $request->request->get('csrf_token'))) {
            $em->remove($genre);
            $em->flush();
            //permet d'afficher un message
            $this->addFlash('info', 'Genre Supprimé!');
        }
        return $this->redirectToRoute('genre');
    }
    #[Route(path: '/genre/{id<[0-9]+>}/edit', name: 'edit_genre', methods: ['GET', 'POST'])]
    #[ParamConverter('genre')]
    public function edit(Request $request, EntityManagerInterface $em, Genre $genre): Response
    {
        $form = $this->createForm(GenreType::class, $genre, [
            'method' => 'PUT',
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            //permet d'afficher un message
            $this->addFlash('success', 'Genre Modifié!');
            return $this->redirectToRoute('genre');
        }
        return $this->render('genre/edit.html.twig', [
            'controller_name' => 'GenreController',
            'genre' => $genre,
            'monForm' => $form->createView(),
        ]);
    }

    #[Route(path: '/genre/create_igdb', name: 'create_genre_igdb', methods: ['GET', 'POST'])]
    public function createFromIgdb(GenreRepository $repo, EntityManagerInterface $em): Response
    {
        $igdb = new IGDB("z4gd9b08838vy37j5v5u7p3v6st0bm", "4tsa0867uzjizuaqq43w22z2cdbhd1");
        $builder = new IGDBQueryBuilder();

        $query =
            //->search((int) $id)
            $builder->sort("genre.name asc")
            ->fields("id, name")
            ->limit(200)
            ->build();
        /*** 
         * @var array $genres
         */
        $genres = $igdb->genre($query);
        $countInsert = 0;
        foreach ($genres as $theGenre) {
            $genre = new Genre;
            $isPresent = $repo->findOneBy(['idIgdb' => $theGenre->id]);
            if (!isset($isPresent)) {
                $genre->setName($theGenre->name);
                $genre->setIdIgdb($theGenre->id);
                $em->persist($genre);
                $em->flush();
                $countInsert++;
            }
        }
        $this->addFlash('success', $countInsert . ' Genres Ajoutés!');
        return $this->redirectToRoute('genre');
    }
}
