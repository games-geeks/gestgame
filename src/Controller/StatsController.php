<?php

namespace App\Controller;

use App\Repository\GameRepository;
use Symfony\UX\Chartjs\Model\Chart;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class StatsController extends AbstractController
{
    #[Route(path: '/stats', name: 'stats')]
    public function index(ChartBuilderInterface $chartBuilder, GameRepository $repo): Response
    {

        $color = [
            '#A6CEE3',
            '#1F78B4',
            '#B2DF8A',
            '#33A02C',
            '#FB9A99',
            '#E31A1C',
            '#FDBF6F',
            '#FF7F00',
            '#6A3D9A',
            '#FFFF99',
            '#B15928'
        ];
        $chartGenre = $this->statByGenre($chartBuilder, $repo, $color);
        $chartPegi = $this->statByPegi($chartBuilder, $repo, $color);
        $chartDate = $this->statByDate($chartBuilder, $repo, $color);
        return $this->render('stats/index.html.twig', [
            'chartGenre' => $chartGenre,
            'chartPegi' => $chartPegi,
            'chartDate' => $chartDate,
        ]);
    }


    private function randomColorPart()
    {
        return str_pad(dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT);
    }

    private function randomCcolor()
    {
        //return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
        return '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
    }

    /**
     * Fonction permettant de générer un graph avec une répartition par genre.
     * @param  Symfony\UX\Chartjs\Builder\ChartBuilderInterface; $chartBuilder :
     *  chartBuilder permettant la création du graph
     * @param App\Repository\GameRepository; $repo         : Permet de gérer les requêtes doctrine
     * @param array $color        : Tableau gérant les couleurs du graph
     * @return Symfony\UX\Chartjs\Model\Chart Chart
     */
    private function statByGenre(ChartBuilderInterface $chartBuilder, GameRepository $repo, $color): Chart
    {
        $results = $repo->groupByGenre();

        $labels = [];
        $data = [];


        foreach ($results as $result) {
            $labels[] = $result['byGenre'];
            $data[] = $result['nb'];
            // $color[] = $this->random_color();
        }
        // dd($color);
        $chart = $chartBuilder->createChart(Chart::TYPE_DOUGHNUT);
        $chart->setData([
            'labels' => $labels,
            'datasets' => [
                [
                    'label' => 'Répartition par Genre',
                    'backgroundColor' => $color,
                    // 'borderColor' => 'rgb(255, 99, 132)',
                    'data' => $data,

                ],
            ],

        ]);
        $chart->setOptions([
            'title' => [
                'text' => 'Répartition des jeux par genres',
                'display' => true,
                'position' => 'bottom',
            ],
            'legend' => [
                'position' => 'right',
            ],
        ]);
        return $chart;
    }

    /**
     * Fonction permettant de générer un graph avec une répartition par Pegi.
     * @param  Symfony\UX\Chartjs\Builder\ChartBuilderInterface; $chartBuilder :
     * chartBuilder permettant la création du graph
     * @param App\Repository\GameRepository; $repo         : Permet de gérer les requêtes doctrine
     * @param array $color        : Tableau gérant les couleurs du graph
     * @return Symfony\UX\Chartjs\Model\Chart Chart
     */
    private function statByPegi(ChartBuilderInterface $chartBuilder, GameRepository $repo, $color): Chart
    {
        $results = $repo->groupByPegi();

        $labels = [];
        $data = [];


        foreach ($results as $result) {
            $labels[] = $result['byPegi'];
            $data[] = $result['nb'];
            // $color[] = $this->random_color();
        }
        // dd($color);
        $chart = $chartBuilder->createChart(Chart::TYPE_DOUGHNUT);
        $chart->setData([
            'labels' => $labels,
            'datasets' => [
                [
                    'label' => 'Répartition par Pegi',
                    'backgroundColor' => $color,
                    // 'borderColor' => 'rgb(255, 99, 132)',
                    'data' => $data,

                ],
            ],

        ]);
        $chart->setOptions([
            'title' => [
                'text' => 'Répartition des jeux par Pegi',
                'display' => true,
                'position' => 'bottom',
            ],
            'legend' => [
                'position' => 'right',
            ],
        ]);
        return $chart;
    }

    /**
     * Fonction permettant de générer un graph avec une répartition par Date d'ajout.
     * @param  Symfony\UX\Chartjs\Builder\ChartBuilderInterface; $chartBuilder :
     * chartBuilder permettant la création du graph
     * @param App\Repository\GameRepository; $repo         : Permet de gérer les requêtes doctrine
     * @param array $color        : Tableau gérant les couleurs du graph
     * @return Symfony\UX\Chartjs\Model\Chart Chart
     */
    private function statByDate(ChartBuilderInterface $chartBuilder, GameRepository $repo, $color): Chart
    {
        $results = $repo->groupByDate();

        $labels = [];
        $data = [];


        foreach ($results as $result) {
            $labels[] = $result['byDate'];
            $data[] = $result['nb'];
            // $color[] = $this->random_color();
        }
        // dd($color);
        $chart = $chartBuilder->createChart(Chart::TYPE_LINE);
        $chart->setData([
            'labels' => $labels,
            'datasets' => [
                [
                    'label' => 'Nombre de jeux ajoutés',
                    'backgroundColor' => $color,
                    // 'borderColor' => 'rgb(255, 99, 132)',
                    'data' => $data,

                ],
            ],

        ]);
        $chart->setOptions([
            'title' => [
                'text' => 'Répartition des jeux par Pegi',
                'display' => true,
                'position' => 'bottom',
            ],
            'legend' => [
                'position' => 'right',
            ],
        ]);
        return $chart;
    }
}
