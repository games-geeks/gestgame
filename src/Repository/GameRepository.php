<?php

namespace App\Repository;

use App\Entity\Game;
use App\Data\SearchData;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManager;

/**
 * @method Game|null find($id, $lockMode = null, $lockVersion = null)
 * @method Game|null findOneBy(array $criteria, array $orderBy = null)
 * @method Game[]    findAll()
 * @method Game[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GameRepository extends ServiceEntityRepository
{

    /**
     * @var PaginatorInterface
     */
    private $paginator;
    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, Game::class);
        $this->paginator = $paginator;
    }
    public function add(Game $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Game $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
    /**
     *
     * @return Game[] Returns an array of GameUser objects filtered by Developpeur
     */
    public function findByDeveloppeurs($value): array
    {
        return
            $this->createQueryBuilder('g')
            ->innerJoin('g.developpeurs', 'd')
            ->where('d.id = :search')
            ->setParameter('search', $value)
            ->getQuery()->getResult();
    }

    /**
     *
     * @return Game[] Returns an array of GameUser objects filtered by Editeur
     */
    public function findByEditeurs($value): array
    {
        return
            $this->createQueryBuilder('g')
            ->innerJoin('g.editeurs', 'e')
            ->where('e.id = :search')
            ->setParameter('search', $value)
            ->getQuery()->getResult();
    }
    /**
     *
     * @return Game[] Returns an array of GameUser objects filtered by Genre
     */
    public function findByGenres($value): array
    {
        return
            $this->createQueryBuilder('g')
            ->innerJoin('g.genres', 'e')
            ->where('e.id = :search')
            ->setParameter('search', $value)
            ->getQuery()->getResult();
    }

    /**
     * Permet de grouper les jeux par genres.
     * @return Game[] Returns an array of Game groupe by Genre
     */
    public function groupByGenre()
    {
        $query = $this->createQueryBuilder('a')
            ->select('g.name as byGenre, COUNT(a) as nb')
            ->innerJoin('a.genres', 'g')
            ->groupBy('byGenre');
        return $query->getQuery()->getResult();
    }

    /**
     * Permet de grouper les jeux par pegi.
     * @return Game[] Returns an array of Game groupe by Pegi
     */
    public function groupByPegi()
    {
        $query = $this->createQueryBuilder('a')
            ->select('g.age as byPegi, COUNT(a) as nb')
            ->innerJoin('a.pegi', 'g')
            ->groupBy('byPegi');
        return $query->getQuery()->getResult();
    }

    /**
     * Permet de grouper les jeux par date d'ajout.
     * @return Game[] Returns an array of Game groupe by Pegi
     */
    public function groupByDate()
    {

        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT 
                DATE_FORMAT(g.createdAt, \'%Y%m\') as byDate, 
                (
                    SELECT COUNT(g2.id) 
                    FROM App\Entity\Game g2 
                    WHERE g2.createdAt <= g.createdAt
                ) as nb
            FROM App\Entity\Game g
            GROUP BY byDate
            ORDER BY byDate ASC'
        );


        return $query->getResult();

        //select creeatedAt , count(*), SUM(COUNT(*)) OVER (ORDER BY DATE(creeatedAt )) from games group by creeatedat ;

        // $query = $this->createQueryBuilder('a')
        //     ->select('a.createdAt as byDate,SUM(COUNT(a)) OVER (ORDER BY DATE(a.createdAt)) AS somme_cumulee')
        //     ->groupBy('byDate');
        // // $query = $entityManager->createNativeQuery();
        // return $query->getQuery()->getResult();
    }

    /**
     * Recherche les jeux en fonction du formulaire
     * @return Game[]
     */
    public function search($mots = null/*, $categorie = null*/)
    {
        $query = $this->createQueryBuilder('a');

        if ($mots != null) {
            $query->andWhere('MATCH_AGAINST(a.title, a.description) AGAINST (:mots boolean)>0')
                ->setParameter('mots', $mots);
        }
        // if ($categorie != null) {
        //     $query->leftJoin('a.categories', 'c');
        //     $query->andWhere('c.id = :id')
        //         ->setParameter('id', $categorie);
        // }
        return $query->getQuery()->getResult();
    }

    // /**
    //  * @return Game[] Returns an array of Game objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Game
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    /**
     * Récupère les jeux en lien avec une recherche
     * @return PaginationInterface
     */
    public function findSearch(SearchData $search, int $nbReturn = 48): PaginationInterface
    {
        $query = $this->getSearchQuery($search)->getQuery();


        $return = $this->paginator->paginate(
            $query,
            $search->getPage(),
            $nbReturn

        );
        $return->setCustomParameters([
            'align' => 'center', # center|right (for template: twitter_bootstrap_v4_pagination)
            'size' => 'small', # small|large (for template: twitter_bootstrap_v4_pagination)
            'style' => 'bottom',
            'span_class' => 'whatever',
        ]);
        return $return;
    }
    // /**
    //  * Récupère le prix minimum et maximum correspondant à une recherche
    //  * @return integer[]
    //  */
    // public function findMinMax(SearchData $search): array
    // {
    //     $results = $this->getSearchQuery($search, true)
    //         ->select('MIN(p.price) as min', 'MAX(p.price) as max')
    //         ->getQuery()
    //         ->getScalarResult();
    //     return [(int)$results[0]['min'], (int)$results[0]['max']];
    // }

    private function getSearchQuery(SearchData $search, $ignorePrice = false): QueryBuilder
    {
        $query = $this
            ->createQueryBuilder('g')
            ->select('g', 'p', 'c', 't')
            ->leftjoin('g.pegi', 'p')
            ->leftjoin('g.consoles', 'c')
            ->leftjoin('g.developpeurs', 'd')
            ->leftjoin('g.genres', 't')
            ->leftjoin('g.editeurs', 'e')
            ->leftjoin('g.serie', 's')
            ->orderBy('g.id', 'DESC');

        if (!empty($search->qSearch)) {
            $query = $query
                ->andWhere('g.title LIKE :qSearch')
                ->setParameter('qSearch', "%{$search->qSearch}%");
        }

        if (!empty($search->consoles)) {
            $query = $query
                ->andWhere('c.id IN (:consoles)')
                ->setParameter('consoles', $search->consoles);
        }

        if (!empty($search->genres)) {
            $query = $query
                ->andWhere('t.id IN (:genres)')
                ->setParameter('genres', $search->genres);
        }

        if (!empty($search->pegis)) {
            $query = $query
                ->andWhere('p.id IN (:pegis)')
                ->setParameter('pegis', $search->pegis);
        }
        if (!empty($search->developpeurs)) {
            $query = $query
                ->andWhere('d.id IN (:developpeurs)')
                ->setParameter('developpeurs', $search->developpeurs);
        }
        if (!empty($search->editeurs)) {
            $query = $query
                ->andWhere('e.id IN (:editeurs)')
                ->setParameter('editeurs', $search->editeurs);
        }
        if (!empty($search->series)) {
            $query = $query
                ->andWhere('s.id IN (:series)')
                ->setParameter('series', $search->series);
        }

        return $query;
    }
}
