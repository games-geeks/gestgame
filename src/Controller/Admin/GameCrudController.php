<?php

namespace App\Controller\Admin;

use App\Entity\Game;
use Vich\UploaderBundle\Form\Type\VichImageType;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;

class GameCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Game::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('title'),
            TextareaField::new('description'),
            DateField::new('releasedate'),
            TextField::new('imageFile')->setFormType(VichImageType::class)->hideOnIndex(),
            ImageField::new('cover')->setBasePath('/uploads/covers/')->onlyOnIndex(),
            AssociationField::new('genres'),
            AssociationField::new('pegi'),
            AssociationField::new('serie'),
            AssociationField::new('editeurs'),
            AssociationField::new('developpeurs'),
            AssociationField::new('consoles'),

        ];
    }
}
