<?php

namespace App\Twig;

use Twig\TwigFilter;
use Twig\TwigFunction;
use Twig\Extension\AbstractExtension;
use Doctrine\ORM\PersistentCollection;

class AppExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('filter_name', [$this, 'doSomething']),
        ];
    }


    public function getFunctions(): array
    {
        return [
            new TwigFunction('pluralize', [$this, 'pluralize']),
            new TwigFunction('entityToArray', [$this, 'entityToArray']),
            new TwigFunction('consolesToString', [$this, 'consolesToString']),
            new TwigFunction('consolesLongToString', [$this, 'consolesLongToString']),
            new TwigFunction('consolesToStringWithLogo', [$this, 'consolesToStringWithLogo']),
            new TwigFunction('devToString', [$this, 'devToString']),
            new TwigFunction('LangageToString', [$this, 'LangageToString']),
            new TwigFunction('categoryToString', [$this, 'categoryToString']),
        ];
    }

    /**
     * @param int count le nombre à controler
     * @param string singular le singuler
     * @param plural optionnel
     *
     * @return string
     */
    public function pluralize(int $count, string $singular, ?string $plural = null): string
    {
        /* Si pas de pluriel, on le contruit*/
        $plural ??=  $singular . 's';
        /* On regarde selon le nombre ce que l'on doit retourner*/
        $res = $count > 1 ? $plural : $singular;

        return $count . ' ' . $res;
    }

    /**
     * @param Entity Entity à transformer
     *
     * @return string
     */
    public function entityToArray(PersistentCollection  $entity): string
    {
        $tempArray = array();
        foreach ($entity as $ent) {
            array_push($tempArray, $ent->getName());
        }
        return implode(", ", $tempArray);
    }

    /**
     * @param Entity Entity à transformer
     *
     * @return string
     */
    public function consolesToString(PersistentCollection  $entity): string
    {
        $tempArray = array();
        foreach ($entity as $ent) {
            array_push($tempArray, $ent->getShortName());
        }
        return implode(", ", $tempArray);
    }

    /**
     * @param Entity Entity à transformer
     *
     * @return string
     */
    public function consolesLongToString(array $tab): string
    {
        $temp = '';
        $tempArrayNintendo = array();
        $tempArrayMicrosoft = array();
        $tempArrayXbox = array();
        $tempArraySony = array();
        foreach ($tab as $ent) {
            switch ($ent->id) {
                case 167:
                    array_push($tempArraySony, 'PS5');
                    break;
                case 48:
                    array_push($tempArraySony, 'PS4');
                    break;
                case 169:
                    array_push($tempArrayXbox, 'XSX');
                    break;
                case 6:
                    array_push($tempArrayMicrosoft, 'PC');
                    break;
                case 130:
                    array_push($tempArrayNintendo, 'Switch');
                    break;
                default:
                    array_push($tempArraySony, $ent->id);
                    break;
            }
        }
        if (count($tempArrayNintendo) > 0) {
            $temp .= "&nbsp; <span class='iconify' data-icon='fa-brands:nintendo-switch' 
            data-inline='true' style='color:red'></span>&nbsp; "
                . implode(", ", $tempArrayNintendo);
        }
        if (count($tempArraySony) > 0) {
            $temp .= "&nbsp; <i class='fab fa-playstation' style='color:blue' ></i>&nbsp; " .
                implode(", ", $tempArraySony);
        }
        if (count($tempArrayXbox) > 0) {
            $temp .= "&nbsp; <i class='fab fa-xbox' style='color:green'></i>&nbsp; " .
                implode(", ", $tempArrayXbox);
        }
        if (count($tempArrayMicrosoft) > 0) {
            $temp .= "&nbsp; <i class='fab fa-windows' style='color:black'></i>&nbsp; " .
                implode(", ", $tempArrayMicrosoft);
        }

        return $temp;
    }

    /**
     * @param Entity Entity à transformer
     *
     * @return string
     */
    public function devToString(PersistentCollection  $entity): string
    {
        $tempArray = array();
        foreach ($entity as $ent) {
            array_push($tempArray, $ent->getName());
        }
        return implode(", ", $tempArray);
    }

    /**
     * @param Entity Entity à transformer
     *
     * @return string
     */
    public function consolesToStringWithLogo(PersistentCollection  $entity): string
    {

        $temp = '';
        $tempArrayNintendo = array();
        $tempArrayMicrosoft = array();
        $tempArrayXbox = array();
        $tempArraySony = array();
        foreach ($entity as $ent) {
            if ($ent->getConstructeur()->getId() == 3) {
                array_push($tempArraySony, $ent->getShortName());
            } elseif ($ent->getConstructeur()->getId() == 1 && $ent->getId() != 9) {
                array_push($tempArrayXbox, $ent->getShortName());
            } elseif ($ent->getConstructeur()->getId() == 1 && $ent->getId() == 9) {
                array_push($tempArrayMicrosoft, $ent->getShortName());
            } elseif ($ent->getConstructeur()->getId() == 4) {
                array_push($tempArrayNintendo, $ent->getShortName());
            }
        }
        if (count($tempArrayNintendo) > 0) {
            $temp .= "&nbsp; <span class='iconify' data-icon='fa-brands:nintendo-switch' 
            data-inline='true' style='color:red'></span>&nbsp; "
                . implode(", ", $tempArrayNintendo);
        }
        if (count($tempArraySony) > 0) {
            $temp .= "&nbsp; <i class='fab fa-playstation' style='color:blue' ></i>&nbsp; " .
                implode(", ", $tempArraySony);
        }
        if (count($tempArrayXbox) > 0) {
            $temp .= "&nbsp; <i class='fab fa-xbox' style='color:green'></i>&nbsp; " .
                implode(", ", $tempArrayXbox);
        }
        if (count($tempArrayMicrosoft) > 0) {
            $temp .= "&nbsp; <i class='fab fa-windows' style='color:black'></i>&nbsp; " .
                implode(", ", $tempArrayMicrosoft);
        }

        return $temp;
    }

    /**
     * @param Entity Entity à transformer
     *
     * @return string
     */
    public function LangageToString(PersistentCollection  $entity): string
    {

        $temp = '';
        $tempArrayEnglish = array();
        $tempArrayFrench = array();
        foreach ($entity as $ent) {
            if ($ent->getLangue()->getIdIgbd() == 7) {
                array_push($tempArrayEnglish, $ent->getSupport()->getId());
            } elseif ($ent->getLangue()->getIdIgbd() == 12) {
                array_push($tempArrayFrench, $ent->getSupport()->getId());
            }
        }
        if (count($tempArrayEnglish) > 0) {
            $temp .= '<tr>';
            $temp .= '<th scope="row">Anglais</th>';
            if (in_array(1, $tempArrayEnglish))
                $temp .= '<td class="text-center"><i class="fa-regular fa-square-check fa-2xs" style="color: #64d345;"></i></td>';
            else
                $temp .= '<td></td>';
            if (in_array(2, $tempArrayEnglish))
                $temp .= '<td class="text-center"><i class="fa-regular fa-square-check fa-2xs" style="color: #64d345;"></i></td>';
            else
                $temp .= '<td></td>';
            if (in_array(3, $tempArrayEnglish))
                $temp .= '<td class="text-center"><i class="fa-regular fa-square-check fa-2xs" style="color: #64d345;"></i></td>';
            else
                $temp .= '<td></td>';
            $temp .= '</tr>';
        }
        if (count($tempArrayFrench) > 0) {
            $temp .= '<tr>';
            $temp .= '<th scope="row">Français</th>';
            if (in_array(1, $tempArrayFrench))
                $temp .= '<td class="text-center"><i class="fa-regular fa-square-check fa-2xs" style="color: #64d345;"></i></td>';
            else
                $temp .= '<td></td>';
            if (in_array(2, $tempArrayFrench))
                $temp .= '<td class="text-center"><i class="fa-regular fa-square-check fa-2xs" style="color: #64d345;"></i></td>';
            else
                $temp .= '<td></td>';
            if (in_array(3, $tempArrayFrench))
                $temp .= '<td class="text-center"><i class="fa-regular fa-square-check fa-2xs" style="color: #64d345;"></i></td>';
            else
                $temp .= '<td></td>';
            $temp .= '</tr>';
        }

        return $temp;
    }

    /**
     * @param Entity Entity à transformer
     *
     * @return string
     */
    public function categoryToString(int $value): string
    {
        switch ($value) {
            case 0:
                $return = "Jeu Principal";
                break;
            case 2:
                $return = "DLC";
                break;
            case 4:
                $return = "Stand Alone";
                break;
            case 8:
                $return = "Remake";
                break;
            case 9:
                $return = "Remaster";
                break;
            case 10:
                $return = "Expanded Game";
                break;
            default:
                $return = $value;
                break;
        }
        return $return;
    }
}
