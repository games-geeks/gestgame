<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\Timestampable;
use App\Repository\ScreenshotRepository;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

#[Vich\Uploadable]
#[ORM\Entity(repositoryClass: ScreenshotRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Screenshot
{
    use Timestampable;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255, nullable: false)]
    private $titre;

    #[ORM\ManyToOne(targetEntity: game::class, inversedBy: 'screenshots')]
    #[ORM\JoinColumn(nullable: false)]
    private $jeu;

    #[Vich\UploadableField(mapping: 'screenshot', fileNameProperty: 'url')]
    #[Assert\Image(maxSize: '8M')]
    private ?File $imageFile = null;

    #[ORM\Column(type: 'string', length: 255)]
    private $url;

    #[ORM\Column(nullable: true)]
    private ?int $igdbId = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getJeu(): ?game
    {
        return $this->jeu;
    }

    public function setJeu(?game $jeu): self
    {
        $this->jeu = $jeu;

        return $this;
    }
    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->setUpdatedAt(new \DateTimeImmutable);
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getIgdbId(): ?int
    {
        return $this->igdbId;
    }

    public function setIgdbId(?int $igdbId): static
    {
        $this->igdbId = $igdbId;

        return $this;
    }
}
