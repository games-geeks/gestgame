<?php

namespace App\Data;

use DateTime;
use IGDBUtils;


class CompagnieIDGB
{
    private $id;
    private $name;
    private $developer;
    private $publisher;



    public function getID(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function isDeveloper(): bool
    {
        return $this->developer;
    }
    public function isPublisher(): bool
    {
        return $this->publisher;
    }

    public function setID(int $id)
    {
        $this->id = $id;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }
    public function setDeveloper(bool $developer)
    {
        $this->developer = $developer;
    }
    public function setPublisher(bool $publisher)
    {
        $this->publisher = $publisher;
    }
}
