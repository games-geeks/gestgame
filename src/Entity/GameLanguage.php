<?php

namespace App\Entity;

use App\Repository\GameLanguageRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GameLanguageRepository::class)]
class GameLanguage
{

    #[ORM\Id]
    #[ORM\ManyToOne(inversedBy: 'gameLanguages')]
    private ?Game $jeu = null;

    #[ORM\Id]
    #[ORM\ManyToOne(inversedBy: 'gameLanguages')]
    private ?Langage $langue = null;

    #[ORM\Id]
    #[ORM\ManyToOne(inversedBy: 'gameLanguages')]
    private ?LangagePosition $support = null;


    public function getJeu(): ?Game
    {
        return $this->jeu;
    }

    public function setJeu(?Game $jeu): static
    {
        $this->jeu = $jeu;

        return $this;
    }

    public function getLangue(): ?Langage
    {
        return $this->langue;
    }

    public function setLangue(?Langage $langue): static
    {
        $this->langue = $langue;

        return $this;
    }

    public function getSupport(): ?LangagePosition
    {
        return $this->support;
    }

    public function setSupport(?LangagePosition $support): static
    {
        $this->support = $support;

        return $this;
    }
}
