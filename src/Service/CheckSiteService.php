<?php

namespace App\Service;

use App\Entity\Prix;
use voku\helper\HtmlDomParser;
use Symfony\Component\Panther\Client;
use Doctrine\ORM\EntityManagerInterface;

class CheckSiteService
{

    public static function checkFnac() //: string
    {
        $client = Client::createChromeClient();
        $crawler = $client->request(
            'GET',
            //'https://www.cdiscount.com/jeux-pc-video-console/ps5/returnal-jeu-ps5/f-1035002-returnal.html'
            'https://jeux-video.fnac.com/a13568436/Resident-Evil-Village-Xbox-Jeu-Xbox-Series-X'
        );
        $fullPageHtml = $crawler->html();
        dd($fullPageHtml);
        $pageH1 = $crawler->filter('f-priceBox')->text();
        echo $pageH1;
    }

    public static function checkAmazon(): string
    {
        $dom = HtmlDomParser::file_get_html("https://www.amazon.fr/Playstation-Returnal-PS5/dp/B08QWCKXFN");
        $version = $dom->find('span[id=priceblock_ourprice]')->plaintext;
        print_r($version);
        if (count($version) == 0) {
            $version = $dom->find('div[id=availability]')->plaintext;

            if (strpos($version[0], "Temporairement en rupture de stock") == 0) {
                return ("Temporairement en rupture de stock");
            }
        }
        $tabVersion[] = explode(' ', $version[0]);
        return trim($tabVersion[0][0]);
    }

    public static function checkAuchan(): string
    {
        $dom = HtmlDomParser::file_get_html(
            "https://www.auchan.fr/it-takes-two-ps4/p-c1363084"
        );
        $version = $dom->find('span.product-price--formattedValue ')->plaintext;
        $tabVersion[] = explode(' ', $version[0]);
        print_r($tabVersion);
        return trim($tabVersion[0][0]);
    }

    public static function checkMicromania(): string
    {
        $dom = HtmlDomParser::file_get_html("https://www.micromania.fr/returnal-107323.html");
        $version = $dom->find('span.sales')->plaintext;
        $tabVersion[] = explode(' ', $version[0]);
        print_r($tabVersion);
        return trim($tabVersion[0][0]);
    }

    public static function checkBoulanger(): string
    {
        $dom = HtmlDomParser::file_get_html("https://www.boulanger.com/ref/1152608#tr=returnal");
        $version = $dom->find('p.fix-price')->plaintext;
        $tabVersion[] = explode(' ', $version[0]);
        print_r($tabVersion);
        return trim($tabVersion[0][0]);
    }

    public static function checkLeclerc(): string
    {
        $dom = HtmlDomParser::file_get_html(
            "https://www.e.leclerc/fp/animal-crossing-new-horizons-switch-0045496425425"
        );
        $version = $dom->find('div[id=price]')->plaintext;
        $tabVersion[] = explode(' ', $version[0]);
        print_r($tabVersion);
        echo $tabVersion[0][0] . $tabVersion[0][2];
        return trim($tabVersion[0][0] . $tabVersion[0][2]);
    }

    public static function checkLeclercChrome() //: string
    {
        $client = Client::createChromeClient();
        $crawler = $client->request(
            'GET',
            "https://www.e.leclerc/fp/animal-crossing-new-horizons-switch-0045496425425"
        );
        $fullPageHtml = $crawler->html();

        dd($crawler->filter('.price'));
        $pageH1 = $crawler->filter('.price'); //-> ->text();
        echo $pageH1;
        return $pageH1;
    }

    public static function checkcDiscount() //: string
    {
        $client = Client::createChromeClient();
        $crawler = $client->request(
            'GET',
            'https://www.cdiscount.com/jeux-pc-video-console/xbox-one/resident-evil-8-village-jeu-xbox-one-et-xbox-serie/f-1030201-re8villagex1xsx.html'
        );
        $fullPageHtml = $crawler->html();
        dd($fullPageHtml);
        $pageH1 = $crawler->filter('.fpPrice')->text();
        echo $pageH1;
    }


    private static function checkPrixPanther(Prix $prix): string
    {
        $client = Client::createChromeClient();
        $crawler = $client->request(
            'GET',
            $prix->getUrl()
        );
        //$fullPageHtml = $crawler->html();
        $pricCourant = $crawler->filter($prix->getEnseigne()->getRecherche())->text();
        return CheckSiteService::cleanPrix(trim($pricCourant));
    }

    private static function checkPrixDom(Prix $prix): string
    {
        $dom = HtmlDomParser::file_get_html($prix->getUrl());
        if ($prix->getEnseigne()->getBalise() == 'plaintext') {
            $version = $dom->find($prix->getEnseigne()->getRecherche())->plaintext;
        } elseif ($prix->getEnseigne()->getBalise() == 'innertext') {
            $version = $dom->find($prix->getEnseigne()->getRecherche())->innertext;
        } else {
            $version = $dom->find($prix->getEnseigne()->getRecherche())->outertext;
        }
        if (count($version) == 0) { // Prix non trouvé, rupture etc...
            $pricCourant = "0";
        } else { //prix trouvé
            $tabVersion[] = explode(' ', $version[0]);
            if (6 == $prix->getEnseigne()->getId()) {
                $pricCourant = CheckSiteService::cleanPrix(trim($tabVersion[0][$prix->getEnseigne()->getChamps()] .  $tabVersion[0][2]));
            } else {
                $pricCourant = CheckSiteService::cleanPrix(trim($tabVersion[0][$prix->getEnseigne()->getChamps()]));
            }
        }

        return $pricCourant;
    }

    public static function checkPrix(Prix $prix, EntityManagerInterface $entityManager)
    {
        //on commence par regarder quel vérif on va faire
        if ($prix->getEnseigne()->getBalise() == 'panther') {
            $returnPrix = CheckSiteService::checkPrixPanther($prix);
        } else {
            $returnPrix = CheckSiteService::checkPrixDom($prix);
        }
        $prix->setEvolution('=');
        if ($returnPrix != $prix->getPrixCourant()) { //évolution du prix
            $prix->setPrixAncien($prix->getPrixCourant());
            $prix->setPrixCourant($returnPrix);
            if ($prix->getPrixCourant() < $returnPrix) {
                $prix->setEvolution('+');
            } elseif ($prix->getPrixCourant() > $returnPrix) {
                $prix->setEvolution('-');
            }
        }
        if ($returnPrix != "0") {
            if (!($prix->getPrixMax()) ||  $returnPrix > $prix->getPrixMax()) {
                $prix->setPrixMax($returnPrix);
            }
            if (!($prix->getPrixMin()) || $returnPrix < $prix->getPrixMin()) {
                $prix->setPrixMin($returnPrix);
            }
        }
        //$entityManager->persist($site);
        $entityManager->flush();
        return $prix;
    }

    public static function cleanPrix(String $prix): float
    {
        //$d = 0.00;

        $version = htmlspecialchars_decode($prix);
        $version = str_replace("€", "", $version);
        $version = str_replace("&euro;", "", $version);
        $version = trim(str_replace(" ", "", $version));
        $version = preg_replace('/\xc2\xa0/', ' ', $version);
        $version = trim(str_replace(",", ".", $version));

        if (strpos($version, '.') == false) {
            return (float) htmlspecialchars_decode($version);
        } else {
            return (float) htmlspecialchars_decode($version) * 100;
        }
    }
}
