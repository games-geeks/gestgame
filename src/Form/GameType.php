<?php

namespace App\Form;

use App\Entity\Game;
use App\Entity\Pegi;
use App\Entity\Genre;
use App\Entity\Serie;
use App\Entity\Console;
use App\Entity\Editeur;
use App\Entity\Developpeur;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;


class GameType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, ['label' => "Nom du Jeu", 'attr' => ['autofocus' => true]])
            ->add('igdbId', IntegerType::class, ['required' => false])
            ->add('releaseDate', DateType::class, [
                'label' => "Date de Sortie",
                'widget' => 'single_text',
                'input'  => 'datetime',
                // 'attr' => ['class' => 'js-datepicker']
            ])
            // ->add('description', CKEditorType::class, ['label' => 'Desc', 'attr' => ['cols' => '60', 'rows' => '10']])
            ->add('description', TextareaType::class, ['label' => 'Desc', 'attr' => ['cols' => '60', 'rows' => '10']])
            ->add('website', UrlType::class, ['label' => "Site Officiel"])

            ->add('consoles', EntityType::class, [
                'class' => Console::class,
                'choice_label' => 'longName',
                'multiple' => true,
                'expanded' => false,
                'placeholder' => 'Choisir Console(s)',
                'query_builder' => function (EntityRepository $repo) {
                    return $repo->createQueryBuilder('d')
                        ->orderBy('d.longName', 'ASC');
                },
                'autocomplete' => true,
            ])
            ->add('genres', EntityType::class, [
                'class' => Genre::class,
                'choice_label' => 'name',
                'multiple' => true,
                'placeholder' => 'Choisir Gerne(s)',
                'query_builder' => function (EntityRepository $repo) {
                    return $repo->createQueryBuilder('d')
                        ->orderBy('d.name', 'ASC');
                },
                'autocomplete' => true,
            ])
            ->add('editeurs', EntityType::class, [
                'class' => Editeur::class,
                'choice_label' => 'name',
                'multiple' => true,
                'placeholder' => 'Choisir Editeur(s)',
                'query_builder' => function (EntityRepository $repo) {
                    return $repo->createQueryBuilder('d')
                        ->orderBy('d.name', 'ASC');
                },
                'autocomplete' => true,
            ])
            ->add('developpeurs', EntityType::class, [
                'class' => Developpeur::class,
                'choice_label' => 'name',
                'multiple' => true,
                'placeholder' => 'Choisir Développeur(s)',
                'query_builder' => function (EntityRepository $repo) {
                    return $repo->createQueryBuilder('d')
                        ->orderBy('d.name', 'ASC');
                },
                'autocomplete' => true,
            ])
            ->add('serie', EntityType::class, [
                'class' => Serie::class,
                'choice_label' => 'name',
                'multiple' => false,
                'placeholder' => 'Choisir Serie',
                'query_builder' => function (EntityRepository $repo) {
                    return $repo->createQueryBuilder('d')
                        ->orderBy('d.name', 'ASC');
                },
                'autocomplete' => true,
            ])
            ->add('pegi', EntityType::class, [
                'class' => Pegi::class,
                'choice_label' => 'age',
                'multiple' => false,
                'placeholder' => 'Choisir PEGI',
                'autocomplete' => true,
            ])

            ->add('imageFile', VichImageType::class, [
                'label' => 'Image (Png or Jpg)',
                'required' => false,
                'allow_delete' => true,
                //'delete_label' => '...',
                //'download_label' => '...',
                'download_uri' => false,
                //'image_uri' => true,
                'imagine_pattern' => 'squared_thumbnail_small',
                //'asset_helper' => true,
                //'constraints' => [
                //                new Image(['maxSize' =>'1M'])
            ])
            //       ->add('slug', TextType::class, ['label' => "Slug"]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Game::class,
        ]);
    }
}
