<?php

namespace App\Command;


use App\Service\OutilIgdbService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:create-language-position',
    description: 'Create a list of language type',
)]
class AddLanguagePositionCommand extends Command
{
    public function __construct(private readonly OutilIgdbService $ois)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->ois->createLanguagePositionFromIGDB();

        $io->success('Successfuly add languages.');

        return Command::SUCCESS;
    }
}
