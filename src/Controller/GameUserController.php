<?php

namespace App\Controller;

/**
 * \file          GameUserController.php
 * \author    Denny
 * \version   1.0
 * \date       24 décembre 2020
 * \brief       Définit les actions de la classe GameUser.
 *
 * \details    Cette classe permet de gérer les objets de la classe GameUser.
 */

use App\Entity\Game;
use App\Data\SearchData;
use App\Entity\GameUser;
use App\Form\GameUserType;
use App\Form\SearchDataType;
use App\Repository\GameUserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class GameUserController extends AbstractController
{
    #[Route(path: '/mygame', name: 'my_game')]
    public function index(GameUserRepository $repo, Request $request): Response
    {


        //on ne peut pas y accéder si pas enregistré
        if (!$this->getUser()) {
            $this->addFlash('error', 'Il faut être authentifié!');
            return $this->redirectToRoute('app_login');
        }

        //$games = $repo->findBy([], ['title' => 'ASC']);
        $data = new SearchData();
        $data->setPage((int) $request->get('page', 1));
        //Ajout de la recherche. A voir si on ne crée pas une page à part
        $form = $this->createForm(
            SearchDataType::class,
            $data,
            [
                'filter' => 'mygame',

            ]
        );
        $form->handleRequest($request);
        $games = $repo->findSearch($data, $this->getUser(), $this->getParameter('app.affich'));



        return $this->render('game_user/index.html.twig', [
            'controller_name' => 'GameUserController',
            'Wish' => '0',
            'games' => $games,
            'form' => $form->createView(),
        ]);
    }

    public function indexold(PaginatorInterface $paginator, GameUserRepository $repo, Request $request): Response
    {

        //on ne peut pas y accéder si pas enregistré
        if (!$this->getUser()) {
            $this->addFlash('error', 'Il faut être authentifié!');
            return $this->redirectToRoute('app_login');
        }

        //$games = $repo->findByUser($this->getUser());
        $games = $repo->findBy(['user' => $this->getUser(), 'wishes' => '0'], ['id' => 'desc']);
        $articles = $paginator->paginate(
            // Doctrine Query, not results
            $games,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            10
        );

        $articles->setCustomParameters([
            'align' => 'center', # center|right (for template: twitter_bootstrap_v4_pagination)
            'size' => 'small', # small|large (for template: twitter_bootstrap_v4_pagination)
            'style' => 'bottom',
            'span_class' => 'whatever',
        ]);

        return $this->render('game_user/index.html.twig', [
            'controller_name' => 'GameUserController',
            'Wish' => '0',
            'games' => $articles,
        ]);
    }

    #[Route(path: '/mygame/wish', name: 'my_wish')]
    public function wish(PaginatorInterface $paginator, GameUserRepository $repo, Request $request): Response
    {

        //on ne peut pas y accéder si pas enregistré
        if (!$this->getUser()) {
            $this->addFlash('error', 'Il faut être authentifié!');
            return $this->redirectToRoute('app_login');
        }

        //$games = $repo->findByUser($this->getUser());
        $games = $repo->findBy(['user' => $this->getUser(), 'wishes' => '1'], ['name' => 'ASC']);
        $articles = $paginator->paginate(
            // Doctrine Query, not results
            $games,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            10
        );

        $articles->setCustomParameters([
            'align' => 'center', # center|right (for template: twitter_bootstrap_v4_pagination)
            'size' => 'small', # small|large (for template: twitter_bootstrap_v4_pagination)
            'style' => 'bottom',
            'span_class' => 'whatever',
        ]);

        return $this->render('game_user/index.html.twig', [
            'Wish' => '1',
            'games' => $articles,
        ]);
    }

    /**
     *
     * @return Response
     */
    #[Route(path: '/mygame/create', name: 'create_mygame', methods: ['GET', 'POST'])]
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        if (!$this->getUser()) {
            $this->addFlash('error', 'Not Connected!');
            return $this->redirectToRoute('app_login');
        }
        $game = new GameUser;
        if ($request->query->get('idGame')) {
            $repo = $em->getRepository(Game::class);
            $refGame = $repo->findOneBy(['id' => $request->query->get('idGame')]);
            $game->setName($refGame->getTitle());
            $game->setGame($refGame);
        }


        $form = $this->createForm(GameUserType::class, $game);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $game->setUser($this->getUser());
            $em->persist($game);
            $em->flush();
            //permet d'afficher un message
            $this->addFlash('success', 'game successfully added!');
            return $this->redirectToRoute('my_game');
        }
        return $this->render('game_user/create.html.twig', [
            'controller_name' => 'GameController',
            'monForm' => $form->createView(),
        ]);
    }

    #[Route(path: '/mygame/{id<[0-9]+>}', name: 'show_mygame', methods: ['GET'])]
    // #[ParamConverter('game_user')]
    public function show(GameUser $game): Response
    {

        return $this->render('game_user/show.html.twig', [
            'controller_name' => 'GameUserController',
            'game' => $game,
        ]);
    }

    #[Route(path: '/mygame/{id<[0-9]+>}', name: 'delete_mygame', methods: ['DELETE'])]
    // #[ParamConverter('game_user')]
    public function delete(Request $request, EntityManagerInterface $em, GameUser $game): Response
    {
        if ($this->isCsrfTokenValid('game_deletion_' . $game->getId(), $request->request->get('csrf_token'))) {
            $em->remove($game);
            $em->flush();
            //permet d'afficher un message
            $this->addFlash('info', 'Jeu Supprimé!');
        }
        return $this->redirectToRoute('my_game');
    }

    #[Route(path: '/mygame/{id<[0-9]+>}/edit', name: 'edit_mygame', methods: ['GET', 'PUT', 'POST'])]
    // #[ParamConverter('game_user')]
    public function edit(Request $request, EntityManagerInterface $em, GameUser $game): Response
    {
        $form = $this->createForm(GameUserType::class, $game, [
            'method' => 'POST',
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            //permet d'afficher un message
            $this->addFlash('success', 'Jeu Modifié!');
            //return $this->redirectToRoute('my_game');
            return $this->redirectToRoute('show_mygame', ['id' => $game->getId()], Response::HTTP_SEE_OTHER);
        }
        return $this->render('game_user/edit.html.twig', [
            'controller_name' => 'GameController',
            'game' => $game,
            'monForm' => $form->createView(),
        ]);
    }
}
