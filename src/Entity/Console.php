<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\Timestampable;
use App\Repository\ConsoleRepository;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;

#[ORM\Table(name: 'consoles')]
#[ORM\Entity(repositoryClass: ConsoleRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Console
{
    use Timestampable;


    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255, unique: true)]
    #[Assert\NotBlank(message: 'Le champ ne peut être vide')]
    private $longName;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank(message: 'Le champ ne peut être vide')]
    private $shortName;

    #[ORM\ManyToOne(targetEntity: Constructeur::class, inversedBy: 'consoles')]
    #[ORM\JoinColumn(nullable: false)]
    private $constructeur;

    #[ORM\ManyToMany(targetEntity: Game::class, mappedBy: 'consoles')]
    private $games;

    #[ORM\OneToMany(targetEntity: GameUser::class, mappedBy: 'consoles')]
    private $gameUsers;
#[ORM\Column(type: 'integer', nullable: true)]
    private $idIgdb;

    public function __construct()
    {
        $this->games = new ArrayCollection();
        $this->gameUsers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLongName(): ?string
    {
        return $this->longName;
    }

    public function setLongName(string $longName): self
    {
        $this->longName = $longName;

        return $this;
    }

    public function getShortName(): ?string
    {
        return $this->shortName;
    }

    public function setShortName(string $shortName): self
    {
        $this->shortName = $shortName;

        return $this;
    }

    public function getConstructeur(): ?Constructeur
    {
        return $this->constructeur;
    }

    public function setConstructeur(?Constructeur $constructeur): self
    {
        $this->constructeur = $constructeur;

        return $this;
    }

    /**
     * @return Collection|Game[]
     */
    public function getGames(): Collection
    {
        return $this->games;
    }

    public function addGame(Game $game): self
    {
        if (!$this->games->contains($game)) {
            $this->games[] = $game;
            $game->addConsole($this);
        }

        return $this;
    }

    public function removeGame(Game $game): self
    {
        if ($this->games->removeElement($game)) {
            $game->removeConsole($this);
        }

        return $this;
    }

    /**
     * @return Collection|GameUser[]
     */
    public function getGameUsers(): Collection
    {
        return $this->gameUsers;
    }

    public function addGameUser(GameUser $gameUser): self
    {
        if (!$this->gameUsers->contains($gameUser)) {
            $this->gameUsers[] = $gameUser;
            $gameUser->setConsoles($this);
        }

        return $this;
    }

    public function removeGameUser(GameUser $gameUser): self
    {
        if ($this->gameUsers->removeElement($gameUser)) {
            // set the owning side to null (unless already changed)
            if ($gameUser->getConsoles() === $this) {
                $gameUser->setConsoles(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getLongName();
    }

    public function getIdIgdb(): ?int
    {
        return $this->idIgdb;
    }

    public function setIdIgdb(?int $idIgdb): self
    {
        $this->idIgdb = $idIgdb;
        return $this;
    }
}
