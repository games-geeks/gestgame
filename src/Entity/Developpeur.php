<?php

namespace App\Entity;

use App\Entity\Traits\Timestampable;
use App\Repository\DeveloppeurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Table(name: 'developpeurs')]
#[ORM\Entity(repositoryClass: DeveloppeurRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Developpeur
{
    use Timestampable;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255, unique: true)]
    #[Assert\NotBlank(message: 'Le nom ne peut être vide')]
    private $name;

    #[ORM\ManyToMany(targetEntity: Game::class, mappedBy: 'developpeurs')]
    private $games;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $idIgdbd;

    public function __construct()
    {
        $this->games = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Game[]
     */
    public function getGames(): Collection
    {
        return $this->games;
    }

    public function addGame(Game $game): self
    {
        if (!$this->games->contains($game)) {
            $this->games[] = $game;
            $game->addDeveloppeur($this);
        }

        return $this;
    }

    public function removeGame(Game $game): self
    {
        if ($this->games->removeElement($game)) {
            $game->removeDeveloppeur($this);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function getIdIgdbd(): ?int
    {
        return $this->idIgdbd;
    }

    public function setIdIgdbd(?int $idIgdbd): self
    {
        $this->idIgdbd = $idIgdbd;

        return $this;
    }
}
