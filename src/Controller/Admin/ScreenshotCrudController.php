<?php

namespace App\Controller\Admin;

use App\Entity\Screenshot;
use Vich\UploaderBundle\Form\Type\VichImageType;

use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ScreenshotCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Screenshot::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnIndex(),
            TextField::new('titre'),
            TextField::new('imageFile')->setFormType(VichImageType::class)->hideOnIndex(),
            ImageField::new('url')->setBasePath('/uploads/screenshots/')->onlyOnIndex(),
            AssociationField::new('jeu'),
        ];
    }
}
