<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210512193052 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE enseignes (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, logo VARCHAR(255) DEFAULT NULL, condition_livraison VARCHAR(255) DEFAULT NULL, recherche VARCHAR(255) DEFAULT NULL, champs INT DEFAULT NULL, balise VARCHAR(255) DEFAULT NULL, action_separateur VARCHAR(255) DEFAULT NULL, action_champ INT DEFAULT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE prix (id INT AUTO_INCREMENT NOT NULL, enseigne_id INT NOT NULL, jeu_id INT NOT NULL, console_id INT NOT NULL, url VARCHAR(255) NOT NULL, prix_courant NUMERIC(8, 2) DEFAULT NULL, prix_ancien NUMERIC(8, 2) DEFAULT NULL, evolution VARCHAR(255) DEFAULT NULL, prix_max NUMERIC(8, 2) DEFAULT NULL, prix_min NUMERIC(8, 2) DEFAULT NULL, is_to_check TINYINT(1) NOT NULL, commentaire VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, INDEX IDX_F7EFEA5E6C2A0A71 (enseigne_id), INDEX IDX_F7EFEA5E8C9E392E (jeu_id), INDEX IDX_F7EFEA5E72F9DD9F (console_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE prix ADD CONSTRAINT FK_F7EFEA5E6C2A0A71 FOREIGN KEY (enseigne_id) REFERENCES enseignes (id)');
        $this->addSql('ALTER TABLE prix ADD CONSTRAINT FK_F7EFEA5E8C9E392E FOREIGN KEY (jeu_id) REFERENCES games (id)');
        $this->addSql('ALTER TABLE prix ADD CONSTRAINT FK_F7EFEA5E72F9DD9F FOREIGN KEY (console_id) REFERENCES consoles (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE prix DROP FOREIGN KEY FK_F7EFEA5E6C2A0A71');
        $this->addSql('DROP TABLE enseignes');
        $this->addSql('DROP TABLE prix');
    }
}
