<?php

namespace App\Command;


use App\Service\OutilIgdbService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:create-language',
    description: 'Create a list of language',
)]
class AddLanguageCommand extends Command
{
    public function __construct(private readonly OutilIgdbService $ois)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->ois->createLanguageFromIGDB();

        $io->success('Successfuly add languages.');

        return Command::SUCCESS;
    }
}
