<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231029194817 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Finalisation gestion langue';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE game_langage');
        $this->addSql('CREATE TABLE game_language (jeu_id INT NOT NULL, langue_id INT NOT NULL, support_id INT NOT NULL, INDEX IDX_7F9F8E938C9E392E (jeu_id), INDEX IDX_7F9F8E932AADBACD (langue_id), INDEX IDX_7F9F8E93315B405 (support_id), PRIMARY KEY(jeu_id, langue_id, support_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE game_language ADD CONSTRAINT FK_7F9F8E938C9E392E FOREIGN KEY (jeu_id) REFERENCES games (id)');
        $this->addSql('ALTER TABLE game_language ADD CONSTRAINT FK_7F9F8E932AADBACD FOREIGN KEY (langue_id) REFERENCES langage (id)');
        $this->addSql('ALTER TABLE game_language ADD CONSTRAINT FK_7F9F8E93315B405 FOREIGN KEY (support_id) REFERENCES langage_position (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE game_language DROP FOREIGN KEY FK_7F9F8E938C9E392E');
        $this->addSql('ALTER TABLE game_language DROP FOREIGN KEY FK_7F9F8E932AADBACD');
        $this->addSql('ALTER TABLE game_language DROP FOREIGN KEY FK_7F9F8E93315B405');
        $this->addSql('DROP TABLE game_language');
    }
}
