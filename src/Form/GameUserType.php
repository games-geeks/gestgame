<?php

namespace App\Form;

use App\Entity\Game;
use App\Entity\Console;
use App\Entity\GameUser;
use App\Entity\StatutJeu;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class GameUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['label' => "Nom du Jeu", 'attr' => ['autofocus' => true]])
            ->add('remarque', TextareaType::class, [
                'label' => 'Commentaire',
                'attr' => ['cols' => '60', 'rows' => '2']
            ])
            ->add('isDigital', CheckboxType::class, [
                'label'    => 'Est ce une version digitale?',
                'required' => false,
            ])
            ->add('haveSteelbook', CheckboxType::class, [
                'label'    => 'Un steelbook?',
                'required' => false,
            ])
            ->add('likes', CheckboxType::class, [
                'label'    => 'Coup de coeur?',
                'required' => false,
            ])
            ->add('wishes', CheckboxType::class, [
                'label'    => 'Liste de Souhait?',
                'required' => false,
            ])
            ->add('price', MoneyType::class)
            ->add('purchaseDate', DateType::class, [
                'label' => "Date d'Achat",
                'widget' => 'single_text',
                'input'  => 'datetime'
            ])
            ->add('game', EntityType::class, [
                'class' => Game::class,
                'choice_label' => 'title',
                'multiple' => false,
                'placeholder' => 'Choose an option',
                'query_builder' => function (EntityRepository $repo) {
                    return $repo->createQueryBuilder('d')
                        ->orderBy('d.title', 'ASC');
                },
                'attr' => [
                    'class' => 'select2-choice'
                ]
            ])
            ->add('consoles', EntityType::class, [
                'class' => Console::class,
                'choice_label' => 'longName',
                'multiple' => false,
                'placeholder' => 'Choose an option',
                'query_builder' => function (EntityRepository $repo) {
                    return $repo->createQueryBuilder('d')
                        ->orderBy('d.longName', 'ASC');
                },
                'attr' => [
                    'class' => 'select2-choice'
                ]
            ])
            ->add('statut', EntityType::class, [
                'class' => StatutJeu::class,
                'choice_label' => 'nom',
                'multiple' => false,
                'required' => false,
                'placeholder' => 'Choose an option',
                'attr' => [
                    'class' => 'select2-choice'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => GameUser::class,
        ]);
    }
}
