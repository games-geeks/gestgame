<?php

namespace App\Form;

use App\Entity\Screenshot;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class ScreenshotType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('titre', TextType::class, ['label' => 'titre de la capture', 'attr' => ['autofocus' => true]])
            ->add('igdbId', IntegerType::class, ['required' => false])
            ->add('imageFile', VichImageType::class, [
                'label' => 'Image (Png or Jpg)',
                'required' => true,
                'allow_delete' => true,
                //'delete_label' => '...',
                //'download_label' => '...',
                'download_uri' => false,
                //'image_uri' => true,
                //'imagine_pattern' => 'squared_thumbnail_small',
                //'asset_helper' => true,
                //'constraints' => [
                //                new Image(['maxSize' =>'1M'])
            ])
            ->add('jeu');
    }
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Screenshot::class,
        ]);
    }
}
