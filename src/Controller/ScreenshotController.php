<?php

namespace App\Controller;

use App\Entity\Game;
use App\Entity\Screenshot;
use App\Form\ScreenshotType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ScreenshotController extends AbstractController
{
    #[Route(path: '/screenshot', name: 'screenshot_index', methods: ['GET'])]
    public function index(): Response
    {
        return $this->render('screenshot/index.html.twig', [
            'controller_name' => 'ScreenshotController',
        ]);
    }

    #[Route(path: '/screenshot/create', name: 'create_screenshot', methods: ['GET', 'POST'])]
    #[Security("is_granted('GAME_MANAGE')")]
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        if (!$this->getUser()) {
            $this->addFlash('error', 'Not Connected!');
            return $this->redirectToRoute('app_login');
        }

        $screenshot = new Screenshot;
        if ($request->query->get('id')) {
            $repo = $em->getRepository(Game::class);
            $refGame = $repo->findOneBy(['id' => $request->query->get('id')]);
            $screenshot->setJeu($refGame);
        }

        $form = $this->createForm(ScreenshotType::class, $screenshot);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em->persist($screenshot);
            $em->flush();
            $id = $screenshot->getJeu()->getId();
            //permet d'afficher un message
            $this->addFlash('success', 'video successfully added!');
            return $this->redirectToRoute('show_game', ['id' => $id]);
        }
        return $this->render('screenshot/create.html.twig', [
            'monForm' => $form->createView(),
        ]);
    }
}
