<?php

namespace App\Controller;

use IGDB;
use IGDBQueryBuilder;
use App\Entity\Developpeur;
use App\Form\DeveloppeurType;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\DeveloppeurRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

#[Security("is_granted('GAME_MANAGE')")]
class DeveloppeurController extends AbstractController
{
    #[Route(path: '/developpeur', name: 'developpeur')]
    public function index(DeveloppeurRepository $repo): Response
    {
        $developpeurs = $repo->findBy([], ['name' => 'ASC']);

        return $this->render('developpeur/index.html.twig', [
            'controller_name' => 'DeveloppeurController',
            'developpeurs' => $developpeurs,
        ]);
    }


    #[Route(path: '/developpeur/create', name: 'create_developpeur', methods: ['GET', 'POST'])]
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        $developpeur = new Developpeur;

        $form = $this->createForm(DeveloppeurType::class, $developpeur);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($developpeur);
            $em->flush();
            //permet d'afficher un message
            $this->addFlash('success', 'Développeur successfully created!');
            return $this->redirectToRoute('developpeur');
        }
        return $this->render('developpeur/create.html.twig', [
            'controller_name' => 'developpeurController',
            'monForm' => $form->createView(),
        ]);
    }

    #[Route(path: '/developpeur/{id<[0-9]+>}', name: 'show_developpeur', methods: ['GET'])]
    #[ParamConverter('developpeur')]
    public function show(Developpeur $developpeur): Response
    {

        return $this->render('developpeur/show.html.twig', [
            'controller_name' => 'developpeurController',
            'developpeur' => $developpeur,
        ]);
    }


    #[Route(path: '/developpeur/{id<[0-9]+>}', name: 'delete_developpeur', methods: ['DELETE'])]
    #[ParamConverter('developpeur')]
    public function delete(Request $request, EntityManagerInterface $em, Developpeur $developpeur): Response
    {
        if ($this->isCsrfTokenValid(
            'developpeur_deletion_' . $developpeur->getId(),
            $request->request->get('csrf_token')
        )) {
            $em->remove($developpeur);
            $em->flush();
            //permet d'afficher un message
            $this->addFlash('info', 'Développeur Supprimé!');
        }
        return $this->redirectToRoute('developpeur');
    }
    #[Route(path: '/developpeur/{id<[0-9]+>}/edit', name: 'edit_developpeur', methods: ['GET', 'POST'])]
    #[ParamConverter('developpeur')]
    public function edit(Request $request, EntityManagerInterface $em, Developpeur $developpeur): Response
    {
        $form = $this->createForm(DeveloppeurType::class, $developpeur, [
            'method' => 'PUT',
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            //permet d'afficher un message
            $this->addFlash('success', 'Developpeur Modifié!');
            return $this->redirectToRoute('developpeur');
        }
        return $this->render('developpeur/edit.html.twig', [
            'controller_name' => 'developpeurController',
            'developpeur' => $developpeur,
            'monForm' => $form->createView(),
        ]);
    }

    #[Route(path: '/developpeur/create_igdb', name: 'create_developpeur_igdb', methods: ['GET', 'POST'])]
    public function createFromIGDB(DeveloppeurRepository $repo, Request $request, EntityManagerInterface $em): Response
    {
        $developpeur = new Developpeur;

        $igdb = new IGDB("z4gd9b08838vy37j5v5u7p3v6st0bm", "4tsa0867uzjizuaqq43w22z2cdbhd1");
        $builder = new IGDBQueryBuilder();
        $offset = 1;
        $countInsert = 0;
        //récupération du nombres de compagnies
        $query =
            $builder
            ->fields("id, name,developed,published")
            ->build();
        $nbCompagnies = $igdb->company($query, true);
        //dd($nbCompagnies->count);
        //fin de la récupération du nombre de compagnie
        while ($offset < $nbCompagnies->count) {
            $query =
                //->search((int) $id)
                $builder
                ->fields("id, name,developed,published")
                ->limit(500)
                ->offset($offset)
                ->build();
            $compagnies = $igdb->company($query);


            foreach ($compagnies as $compagnie) {
                $developpeur = new Developpeur;
                $isPresent = $repo->findOneBy(['idIgdbd' => $compagnie->id]);
                if (!isset($isPresent) && isset($compagnie->developed)) {
                    $developpeur->setName($compagnie->name);
                    $developpeur->setIdIgdbd($compagnie->id);
                    $em->persist($developpeur);
                    $em->flush();
                    $countInsert++;
                }
            }
            $offset += 500;
        }
        $this->addFlash('success', $countInsert . ' Développeurs Ajoutés!');
        return $this->redirectToRoute('developpeur');
    }
}
