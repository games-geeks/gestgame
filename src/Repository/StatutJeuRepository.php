<?php

namespace App\Repository;

use App\Entity\StatutJeu;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<StatutJeu>
 *
 * @method StatutJeu|null find($id, $lockMode = null, $lockVersion = null)
 * @method StatutJeu|null findOneBy(array $criteria, array $orderBy = null)
 * @method StatutJeu[]    findAll()
 * @method StatutJeu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatutJeuRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StatutJeu::class);
    }

//    /**
//     * @return StatutJeu[] Returns an array of StatutJeu objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('s.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?StatutJeu
//    {
//        return $this->createQueryBuilder('s')
//            ->andWhere('s.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
