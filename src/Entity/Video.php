<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\VideoRepository;
use App\Entity\Traits\Timestampable;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;

#[ORM\Table(name: 'videos')]
#[ORM\Entity(repositoryClass: VideoRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Video
{
    use Timestampable;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $idYoutube;

    #[ORM\Column(type: 'string', length: 255)]
    private $typeVideo;

    #[ORM\ManyToOne(targetEntity: Game::class, inversedBy: 'videos')]
    #[ORM\JoinColumn(nullable: false)]
    private $jeu;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdYoutube(): ?string
    {
        return $this->idYoutube;
    }

    public function setIdYoutube(string $idYoutube): self
    {
        $this->idYoutube = $idYoutube;

        return $this;
    }

    public function getTypeVideo(): ?string
    {
        return $this->typeVideo;
    }

    public function setTypeVideo(string $typeVideo): self
    {
        $this->typeVideo = $typeVideo;

        return $this;
    }

    public function getJeu(): ?game
    {
        return $this->jeu;
    }

    public function setJeu(?game $jeu): self
    {
        $this->jeu = $jeu;

        return $this;
    }
}
