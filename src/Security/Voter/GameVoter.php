<?php

namespace App\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class GameVoter extends Voter
{
    protected function supports(string $attribute, mixed $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return $attribute === 'GAME_MANAGE' || in_array($attribute, ['GAME_ADD'])
            && $subject instanceof \App\Entity\Game;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case 'GAME_ADD':
                return $user->isVerified();
                // return true or false
                break;
            case 'GAME_MANAGE':
                return  in_array('ROLE_ADMIN', $user->getRoles());
                break;
        }

        return false;
    }
}
