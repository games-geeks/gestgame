<?php

namespace App\Controller;

use IGDB;
use IGDBQueryBuilder;
use App\Entity\Editeur;
use App\Form\EditeurType;
use App\Repository\EditeurRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

#[Security("is_granted('GAME_MANAGE')")]
class EditeurController extends AbstractController
{
    #[Route(path: '/editeur', name: 'editeur')]
    public function index(EditeurRepository $repo): Response
    {
        $editeurs = $repo->findBy([], ['name' => 'ASC']);

        return $this->render('editeur/index.html.twig', [
            'controller_name' => 'EditeurController',
            'editeurs' => $editeurs,
        ]);
    }


    #[Route(path: '/editeur/create', name: 'create_editeur', methods: ['GET', 'POST'])]
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        $editeur = new Editeur;

        $form = $this->createForm(EditeurType::class, $editeur);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($editeur);
            $em->flush();
            //permet d'afficher un message
            $this->addFlash('success', 'Editeur successfully created!');
            return $this->redirectToRoute('editeur');
        }
        return $this->render('editeur/create.html.twig', [
            'controller_name' => 'editeurController',
            'monForm' => $form->createView(),
        ]);
    }

    #[Route(path: '/editeur/{id<[0-9]+>}', name: 'show_editeur', methods: ['GET'])]
    #[ParamConverter('editeur')]
    public function show(Editeur $editeur): Response
    {

        return $this->render('editeur/show.html.twig', [
            'controller_name' => 'editeurController',
            'editeur' => $editeur,
        ]);
    }


    #[Route(path: '/editeur/{id<[0-9]+>}', name: 'delete_editeur', methods: ['DELETE'])]
    #[ParamConverter('editeur')]
    public function delete(Request $request, EntityManagerInterface $em, Editeur $editeur): Response
    {
        if ($this->isCsrfTokenValid('editeur_deletion_' . $editeur->getId(), $request->request->get('csrf_token'))) {
            $em->remove($editeur);
            $em->flush();
            //permet d'afficher un message
            $this->addFlash('info', 'Editeur Supprimé!');
        }
        return $this->redirectToRoute('editeur');
    }
    #[Route(path: '/editeur/{id<[0-9]+>}/edit', name: 'edit_editeur', methods: ['GET', 'POST'])]
    #[ParamConverter('editeur')]
    public function edit(Request $request, EntityManagerInterface $em, Editeur $editeur): Response
    {
        $form = $this->createForm(EditeurType::class, $editeur, [
            'method' => 'PUT',
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            //permet d'afficher un message
            $this->addFlash('success', 'Editeur Modifié!');
            return $this->redirectToRoute('editeur');
        }
        return $this->render('editeur/edit.html.twig', [
            'controller_name' => 'editeurController',
            'editeur' => $editeur,
            'monForm' => $form->createView(),
        ]);
    }

    #[Route(path: '/editeur/create_igdb', name: 'create_editeur_igdb', methods: ['GET', 'POST'])]
    public function createFromIGDB(EditeurRepository $repo, Request $request, EntityManagerInterface $em): Response
    {


        $igdb = new IGDB("z4gd9b08838vy37j5v5u7p3v6st0bm", "4tsa0867uzjizuaqq43w22z2cdbhd1");
        $builder = new IGDBQueryBuilder();
        $offset = 1;
        $countInsert = 0;
        //récupération du nombres de compagnies
        $query =
            $builder
            ->fields("id, name,developed,published")
            ->build();
        $nbCompagnies = $igdb->company($query, true);
        //dd($nbCompagnies->count);
        //fin de la récupération du nombre de compagnie
        while ($offset < $nbCompagnies->count) {
            $query =
                //->search((int) $id)
                $builder
                ->fields("id, name,developed,published")
                ->limit(500)
                ->offset($offset)
                ->build();
            $compagnies = $igdb->company($query);


            foreach ($compagnies as $compagnie) {
                $editeur = new Editeur;
                $isPresent = $repo->findOneBy(['idIgdbd' => $compagnie->id]);
                if (!isset($isPresent) && isset($compagnie->published)) {
                    $editeur->setName($compagnie->name);
                    $editeur->setIdIgdbd($compagnie->id);
                    $em->persist($editeur);
                    $em->flush();
                    $countInsert++;
                }
            }
            $offset += 500;
        }
        $this->addFlash('success', $countInsert . ' Editeurs Ajoutés!');
        return $this->redirectToRoute('editeur');
    }
}
