<?php

namespace App\Form;

use App\Entity\Pegi;



use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class PegiType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('age', IntegerType::class)
            ->add('description', TextareaType::class)
            ->add('imageFile', VichImageType::class, [
                'label' => 'Image (Png or Jpg)',
                'required' => false,
                'allow_delete' => true,
                //'delete_label' => '...',
                //'download_label' => '...',
                'download_uri' => false,
                //'image_uri' => true,
                'imagine_pattern' => 'squared_thumbnail_small',
                //'asset_helper' => true,
                //'constraints' => [
                //                new Image(['maxSize' =>'1M'])
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Pegi::class,
        ]);
    }
}
