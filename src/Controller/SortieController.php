<?php

namespace App\Controller;

use IGDB;
use DateTime;
use IGDBUtils;
use IGDBQueryBuilder;
use App\Data\GameIGDB;
use function Symfony\Component\Clock\now;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SortieController extends AbstractController
{
    #[Route('/sorties_futures', name: 'next_release')]
    public function index(): Response
    {
        $cle = IGDBUtils::authenticate($this->getParameter('igdb.client'), $this->getParameter('igdb.token'));
        //dd($cle->access_token);
        $igdb = new IGDB($this->getParameter('igdb.client'), $cle->access_token);
        //$igdb = new IGDB("z4gd9b08838vy37j5v5u7p3v6st0bm", "4tsa0867uzjizuaqq43w22z2cdbhd1");
        $builder = new IGDBQueryBuilder();
        $result = array();

        // Obtenir la date du jour
        $dateDuJour = date('Y-m-d');

        // Extraire l'année et le mois de la date du jour
        list($annee, $mois) = explode('-', $dateDuJour);

        // Créer un objet DateTime représentant le premier jour du prochain mois
        $premierJourDuProchainMois = new DateTime($annee . '-' . ($mois + 1) . '-01');

        // Obtenir le nombre de jours dans le prochain mois
        $nombreDeJoursDansLeProchainMois = cal_days_in_month(CAL_GREGORIAN, ($mois + 1), $annee);
        // dd($premierJourDuProchainMois, $nombreDeJoursDansLeProchainMois);

        // your query string
        // $query = 'search "Forza Horizon 5"; fields id,name,cover; limit 5; offset 10;';
        $debutDuMois = strtotime('first day of this month');
        $finDuMois = strtotime('last day of this month');
        // $debutDuMois = strtotime('first day of next month');
        // $finDuMois = strtotime('last day of next month');
        // dd($debutDuMois);
        $query = $builder
            ->where("first_release_date >= " . $debutDuMois)
            ->where("first_release_date <= " . $finDuMois)
            ->where("category = (0,2,4,8,9)")
            ->where("version_parent = null")
            ->fields("id, name,first_release_date,cover.image_id,storyline,slug,summary, category, platforms.name")
            ->limit(300)
            ->sort("first_release_date asc")
            ->build();
        //dd($query);
        // executing the query
        $games = $igdb->game($query);

        //dd($games);

        foreach ($games as $game) {
            // dd($game);
            $g  = new GameIGDB();
            $g->setID($game->id);
            $g->setName($game->name);
            $g->setSlug($game->slug);
            if (isset($game->platforms))
                $g->setConsole($game->platforms);
            if (isset($game->category))
                $g->setCategory($game->category);
            if (isset($game->storyline)) {
                $g->setStoryline($game->storyline);
            } else if (isset($game->summary)) {
                $g->setStoryline($game->summary);
            } else
                $g->setStoryline("Description à venir");
            if (isset($game->cover)) {
                $g->setCover($game->cover->image_id);
            }
            //$firstDate->setTimestamp();
            if (isset($game->first_release_date))
                $g->setReleaseDate($game->first_release_date);
            array_push($result, $g);
        }
        return $this->render('search_igdb/retour_igdb.html.twig', [
            'games' => $result,
        ]);
    }
}
