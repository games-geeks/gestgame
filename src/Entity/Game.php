<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\GameRepository;
use App\Entity\Traits\Timestampable;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


#[Vich\Uploadable]
#[ORM\Table(name: 'games')]
#[ORM\Index(columns: ['title', 'description'], flags: ['fulltext'])]
#[ORM\Entity(repositoryClass: GameRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Game
{
    use Timestampable;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255, unique: true)]
    #[Assert\NotBlank(message: 'Le nom ne peut être vide')]
    private $title;

    #[ORM\Column(type: 'text')]
    #[Assert\NotBlank(message: 'La description ne peut être vide')]
    private $description;


    #[Vich\UploadableField(mapping: 'cover_game', fileNameProperty: 'cover')]
    #[Assert\Image(maxSize: '8M')]
    private ?File $imageFile = null;


    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $cover;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $releaseDate;

    #[ORM\ManyToMany(targetEntity: Genre::class, inversedBy: 'games')]
    private $genres;

    #[ORM\ManyToMany(targetEntity: Editeur::class, inversedBy: 'games')]
    private $editeurs;

    #[ORM\ManyToMany(targetEntity: Developpeur::class, inversedBy: 'games')]
    private $developpeurs;

    #[ORM\ManyToMany(targetEntity: Console::class, inversedBy: 'games')]
    private $consoles;

    #[ORM\OneToMany(targetEntity: GameUser::class, mappedBy: 'game')]
    private $gameUsers;

    #[ORM\ManyToOne(targetEntity: Pegi::class, inversedBy: 'game')]
    private $pegi;

    #[ORM\ManyToOne(targetEntity: Serie::class, inversedBy: 'games')]
    private $serie;
    #[ORM\OneToMany(targetEntity: Prix::class, mappedBy: 'jeu')]
    private $prixes;

    #[ORM\Column(type: 'string', length: 255, nullable: true, unique: true)]
    private $slug;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $website;

    #[ORM\OneToMany(targetEntity: Video::class, mappedBy: 'jeu', cascade: ['persist', 'remove'])]
    private $videos;

    #[ORM\OneToMany(targetEntity: Screenshot::class, mappedBy: 'jeu', cascade: ['persist', 'remove'])]
    private $screenshots;

    #[ORM\Column(nullable: true)]
    private ?int $igdbId = null;

    #[ORM\ManyToMany(targetEntity: Langage::class, inversedBy: 'games')]
    private Collection $langage;

    #[ORM\OneToMany(mappedBy: 'jeu', targetEntity: GameLanguage::class)]
    private Collection $gameLanguages;


    public function __construct()
    {
        $this->genres = new ArrayCollection();
        $this->editeurs = new ArrayCollection();
        $this->developpeurs = new ArrayCollection();
        $this->consoles = new ArrayCollection();
        $this->gameUsers = new ArrayCollection();
        $this->videos = new ArrayCollection();
        $this->screenshots = new ArrayCollection();
        $this->gameLanguages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->setUpdatedAt(new \DateTimeImmutable);
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }
    public function getCover(): ?string
    {
        return $this->cover;
    }

    public function setCover(?string $cover): self
    {
        $this->cover = $cover;

        return $this;
    }

    public function getReleaseDate(): ?\DateTimeInterface
    {
        return $this->releaseDate;
    }

    public function setReleaseDate(?\DateTimeInterface $releaseDate): self
    {
        $this->releaseDate = $releaseDate;

        return $this;
    }

    /**
     * @return Collection|Genre[]
     */
    public function getGenres(): Collection
    {
        return $this->genres;
    }

    public function addGenre(Genre $genre): self
    {
        if (!$this->genres->contains($genre)) {
            $this->genres[] = $genre;
        }

        return $this;
    }

    public function removeGenre(Genre $genre): self
    {
        $this->genres->removeElement($genre);

        return $this;
    }

    /**
     * @return Collection|Editeur[]
     */
    public function getEditeurs(): Collection
    {
        return $this->editeurs;
    }

    public function addEditeur(Editeur $editeur): self
    {
        if (!$this->editeurs->contains($editeur)) {
            $this->editeurs[] = $editeur;
        }

        return $this;
    }

    public function removeEditeur(Editeur $editeur): self
    {
        $this->editeurs->removeElement($editeur);

        return $this;
    }

    /**
     * @return Collection|Developpeur[]
     */
    public function getDeveloppeurs(): Collection
    {
        return $this->developpeurs;
    }

    public function addDeveloppeur(Developpeur $developpeur): self
    {
        if (!$this->developpeurs->contains($developpeur)) {
            $this->developpeurs[] = $developpeur;
        }

        return $this;
    }

    public function removeDeveloppeur(Developpeur $developpeur): self
    {
        $this->developpeurs->removeElement($developpeur);

        return $this;
    }

    /**
     * @return Collection|Console[]
     */
    public function getConsoles(): Collection
    {
        return $this->consoles;
    }

    public function addConsole(Console $console): self
    {
        if (!$this->consoles->contains($console)) {
            $this->consoles[] = $console;
        }

        return $this;
    }

    public function removeConsole(Console $console): self
    {
        $this->consoles->removeElement($console);

        return $this;
    }

    /**
     * @return Collection|GameUser[]
     */
    public function getGameUsers(): Collection
    {
        return $this->gameUsers;
    }

    public function addGameUser(GameUser $gameUser): self
    {
        if (!$this->gameUsers->contains($gameUser)) {
            $this->gameUsers[] = $gameUser;
            $gameUser->setGame($this);
        }

        return $this;
    }

    public function removeGameUser(GameUser $gameUser): self
    {
        if ($this->gameUsers->removeElement($gameUser)) {
            // set the owning side to null (unless already changed)
            if ($gameUser->getGame() === $this) {
                $gameUser->setGame(null);
            }
        }

        return $this;
    }

    public function getPegi(): ?Pegi
    {
        return $this->pegi;
    }

    public function setPegi(?Pegi $pegi): self
    {
        $this->pegi = $pegi;

        return $this;
    }

    public function getSerie(): ?Serie
    {
        return $this->serie;
    }

    public function setSerie(?Serie $serie): self
    {
        $this->serie = $serie;

        return $this;
    }

    /**
     * @return Collection|Prix[]
     */
    public function getPrixes(): Collection
    {
        return $this->prixes;
    }

    public function addPrix(Prix $prix): self
    {
        if (!$this->prixes->contains($prix)) {
            $this->prixes[] = $prix;
            $prix->setJeu($this);
        }

        return $this;
    }

    public function removePrix(Prix $prix): self
    {
        if ($this->prixes->removeElement($prix)) {
            // set the owning side to null (unless already changed)
            if ($prix->getJeu() === $this) {
                $prix->setJeu(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getTitle();
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    /**
     * @return Collection|Video[]
     */
    public function getVideos(): Collection
    {
        return $this->videos;
    }

    public function addVideo(Video $video): self
    {
        if (!$this->videos->contains($video)) {
            $this->videos[] = $video;
            $video->setJeu($this);
        }

        return $this;
    }

    public function removeVideo(Video $video): self
    {
        if ($this->videos->removeElement($video)) {
            // set the owning side to null (unless already changed)
            if ($video->getJeu() === $this) {
                $video->setJeu(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Screenshot[]
     */
    public function getScreenshots(): Collection
    {
        return $this->screenshots;
    }

    public function addScreenshot(Screenshot $screenshot): self
    {
        if (!$this->screenshots->contains($screenshot)) {
            $this->screenshots[] = $screenshot;
            $screenshot->setJeu($this);
        }

        return $this;
    }

    public function removeScreenshot(Screenshot $screenshot): self
    {
        if ($this->screenshots->removeElement($screenshot)) {
            // set the owning side to null (unless already changed)
            if ($screenshot->getJeu() === $this) {
                $screenshot->setJeu(null);
            }
        }

        return $this;
    }

    public function getIgdbId(): ?int
    {
        return $this->igdbId;
    }

    public function setIgdbId(?int $igdbId): static
    {
        $this->igdbId = $igdbId;

        return $this;
    }

    /**
     * @return Collection<int, GameLanguage>
     */
    public function getGameLanguages(): Collection
    {
        return $this->gameLanguages;
    }

    public function addGameLanguage(GameLanguage $gameLanguage): static
    {
        if (!$this->gameLanguages->contains($gameLanguage)) {
            $this->gameLanguages->add($gameLanguage);
            $gameLanguage->setJeu($this);
        }

        return $this;
    }

    public function removeGameLanguage(GameLanguage $gameLanguage): static
    {
        if ($this->gameLanguages->removeElement($gameLanguage)) {
            // set the owning side to null (unless already changed)
            if ($gameLanguage->getJeu() === $this) {
                $gameLanguage->setJeu(null);
            }
        }

        return $this;
    }
}
