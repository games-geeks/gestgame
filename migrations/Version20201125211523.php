<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201125211523 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout relation developpeur/game';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE game_developpeur (game_id INT NOT NULL, developpeur_id INT NOT NULL, INDEX IDX_FCA582C5E48FD905 (game_id), INDEX IDX_FCA582C584E66085 (developpeur_id), PRIMARY KEY(game_id, developpeur_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE game_developpeur ADD CONSTRAINT FK_FCA582C5E48FD905 FOREIGN KEY (game_id) REFERENCES games (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE game_developpeur ADD CONSTRAINT FK_FCA582C584E66085 FOREIGN KEY (developpeur_id) REFERENCES developpeurs (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE game_developpeur');
    }
}
