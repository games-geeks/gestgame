<?php

namespace App\Service;

class PhotoCollageService
{
    public function createCollage(array $imagePaths, int $cols, int $rows, string $outputFile,  string $format = 'jpg', int $spacing = 5): string
    {
        // Vérifier si le format est valide
        if (!in_array($format, ['jpg', 'jpeg', 'png', 'webp'])) {
            throw new \InvalidArgumentException('Format non supporté. Utilisez jpg,jpeg, png ou webp.');
        }

        // Stocker les images converties et initialiser les dimensions
        $images = [];
        $width = 0;
        $height = 0;

        // Convertir les images dans le même format
        foreach ($imagePaths as $path) {
            $image = $this->convertImageToFormat($path, $format, $outputFile);
            $images[] = $image;
            if ($width == 0 && $height == 0) {
                $width = imagesx($image);
                $height = imagesy($image);
            }
        }
        dd('fa');
        // Calculer les dimensions totales du collage
        $collageWidth = ($width * $cols) + ($spacing * ($cols - 1));
        $collageHeight = ($height * $rows) + ($spacing * ($rows - 1));

        // Créer une nouvelle image vide pour le collage
        $collage = imagecreatetruecolor($collageWidth, $collageHeight);
        $white = imagecolorallocate($collage, 255, 255, 255); // Fond blanc
        imagefill($collage, 0, 0, $white);

        // Placer chaque image dans le collage
        for ($i = 0; $i < count($images); $i++) {
            $row = floor($i / $cols);
            $col = $i % $cols;

            // Calculer les coordonnées pour placer l'image
            $x = $col * ($width + $spacing);
            $y = $row * ($height + $spacing);

            // Copier l'image dans le collage
            imagecopy($collage, $images[$i], $x, $y, 0, 0, $width, $height);
        }

        // Sauvegarder l'image finale dans un fichier
        $outputPath = $outputFile;

        // Sauvegarder l'image selon le format choisi
        switch ($format) {
            case 'jpeg':
                imagejpeg($collage, $outputPath);
                break;
            case 'png':
                imagepng($collage, $outputPath);
                break;
            case 'webp':
                imagewebp($collage, $outputPath);
                break;
        }

        // Libérer la mémoire
        foreach ($images as $image) {
            imagedestroy($image);
        }
        imagedestroy($collage);

        // Retourner le chemin du fichier
        return $outputPath;
    }

    private function convertImageToFormat(string $path, string $format, string $outputFile)
    {

        // Charger l'image quelle que soit son format actuel
        $options = [
            "ssl" => [
                "verify_peer" => false,
                "verify_peer_name" => false,
            ],
        ];
        $context = stream_context_create($options);
        $image = imagecreatefromstring(file_get_contents($path, false, $context));

        // Créer une image temporaire pour la conversion

        $tmpPath = $path; //$outputFile; //. basename($path, '.' . pathinfo($path, PATHINFO_EXTENSION)) . '.' . $format;
        // dd($path, $outputFile);
        // Sauvegarder l'image dans le format spécifié
        switch ($format) {
            case 'jpeg':
                imagejpeg($image, $tmpPath);
                break;
            case 'png':
                imagepng($image, $tmpPath);
                break;
            case 'webp':
                imagewebp($image, $tmpPath);
                break;
        }

        // Charger l'image convertie à partir du fichier temporaire
        $convertedImage = imagecreatefromstring(file_get_contents($tmpPath, false, $context));
        // dd($path, $outputFile);
        // Supprimer le fichier temporaire
        // unlink($outputFile);
        dd($convertedImage);
        // Retourner l'image convertie
        return $convertedImage;
    }
}
