<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class SearchIGDBType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if ($options['search_controller'] == 'outils') {

            $builder->add('in', ChoiceType::class, [
                'choices'  => [
                    'Console' => 'console',
                    'Editeur/Developpeur' => 'company',
                    'Langage' => 'language',
                    'Langage Type' => 'languageType',

                ],
            ]);
        }
        $builder
            ->add('search', TextType::class, [
                'label' => true,
                'attr' => [
                    'placeholder' => $options['search_label']
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'search_controller' => '',
            'search_label' => 'Entrer le nom du jeu à rechercher'
            // Configure your form options here
        ]);
        $resolver->setAllowedTypes('search_controller', 'string');
        $resolver->setAllowedTypes('search_label', 'string');
    }
}
