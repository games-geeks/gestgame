<?php

namespace App\Form;


use App\Entity\Pegi;
use App\Entity\Genre;
use App\Entity\Console;
use App\Data\SearchData;
use App\Entity\StatutJeu;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;


class SearchDataType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('qSearch', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' =>  [
                    'placeholder' => 'Rechercher'
                ]
            ])
            // ->add('consoles', EntityType::class, [
            //     'label' => false,
            //     'required' => false,
            //     'class' => Console::class,
            //     'expanded' => false,
            //     'multiple' => true,
            //     'query_builder' => function (EntityRepository $repo) {
            //         return $repo->createQueryBuilder('d')
            //             ->orderBy('d.longName', 'ASC');
            //     },
            //     'attr' => [
            //         'class' => 'select2-choice'
            //     ]
            // ])
            ->add('consoles', EntityType::class, [
                'class' => Console::class,
                'choice_label' => 'longName',
                'label' => false,
                'required' => false,
                'multiple' => true,
                'expanded' => false,
                'placeholder' => 'Choisir Console(s)',
                'query_builder' => function (EntityRepository $repo) {
                    return $repo->createQueryBuilder('d')
                        ->orderBy('d.longName', 'ASC');
                },
                'autocomplete' => true,
            ])
            ->add('pegis', EntityType::class, [
                'label' => false,
                'required' => false,
                'class' => Pegi::class,
                'required' => false,
                'expanded' => false,
                'multiple' => true,
                'autocomplete' => true,
            ]);

        if ($options['filter'] == '') {
            $builder
                ->add('genres', EntityType::class, [
                    'class' => Genre::class,
                    'label' => false,
                    'choice_label' => 'name',
                    'multiple' => true,
                    'required' => false,
                    'placeholder' => 'Choisir Gerne(s)',
                    'query_builder' => function (EntityRepository $repo) {
                        return $repo->createQueryBuilder('d')
                            ->orderBy('d.name', 'ASC');
                    },
                    'autocomplete' => true,
                ]);
        }
        if ($options['filter'] != '') {
            $builder
                ->add('isDemat', CheckboxType::class, [
                    'label' => 'Dématérialisé',
                    'required' => false,
                ])
                ->add('isWish', CheckboxType::class, [
                    'label' => 'Souhait',
                    'required' => false,
                ])
                ->add('isLike', CheckboxType::class, [
                    'label' => 'Favori',
                    'required' => false,
                ])
                ->add('statutJeux', EntityType::class, [
                    'class' => StatutJeu::class,
                    'label' => false,
                    'choice_label' => 'nom',
                    'multiple' => true,
                    'required' => false,
                    'query_builder' => function (EntityRepository $repo) {
                        return $repo->createQueryBuilder('d')
                            ->orderBy('d.nom', 'ASC');
                    },
                    'autocomplete' => true,
                ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SearchData::class,
            'method' => 'GET',
            'filter' => '',
            'csrf_protection' => false //car formulaire de recherche
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
