<?php

namespace App\Controller;

use App\Entity\Developpeur;
use App\Entity\Editeur;
use IGDB;
use DateTime;
use App\Entity\Game;
use App\Entity\Screenshot;
use App\Entity\Video;
use IGDBQueryBuilder;
use App\Data\GameIGDB;
use App\Entity\GameLanguage;
use App\Form\SearchIGDBType;
use App\Repository\ConsoleRepository;
use App\Repository\DeveloppeurRepository;
use App\Repository\EditeurRepository;
use App\Repository\GameLanguageRepository;
use App\Repository\GameRepository;
use App\Repository\GenreRepository;
use App\Repository\LangagePositionRepository;
use App\Repository\LangageRepository;
use App\Repository\PegiRepository;
use App\Repository\ScreenshotRepository;
use App\Repository\VideoRepository;
use Doctrine\ORM\EntityManagerInterface;
use IGDBUtils;
use Stichoza\GoogleTranslate\GoogleTranslate;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SearchIGDBController extends AbstractController
{
    #[Route(path: '/igdb', name: 'search_igdb')]
    public function index(Request $request): Response
    {

        $form = $this->createForm(SearchIGDBType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $cle = IGDBUtils::authenticate($this->getParameter('igdb.client'), $this->getParameter('igdb.token'));
            //dd($cle->access_token);
            $igdb = new IGDB($this->getParameter('igdb.client'), $cle->access_token);
            //$igdb = new IGDB("z4gd9b08838vy37j5v5u7p3v6st0bm", "4tsa0867uzjizuaqq43w22z2cdbhd1");
            $builder = new IGDBQueryBuilder();
            $result = array();

            // your query string
            // $query = 'search "Forza Horizon 5"; fields id,name,cover; limit 5; offset 10;';
            $query = $builder
                ->search($form->get('search')->getData())
                ->where("category = (0,1,2,4,8,9,10)")
                ->where("version_parent = null")
                ->fields("id, name,first_release_date,cover.image_id,storyline,slug,summary, category, platforms.name")
                ->limit(30)
                ->build();

            // executing the query
            /**
             * @var Game $games
             */
            $games = $igdb->game($query);

            //dd($games);
            foreach ($games as $game) {
                // dd($game);
                $g  = new GameIGDB();
                $g->setID($game->id);
                $g->setName($game->name);
                $g->setSlug($game->slug);
                if (isset($game->platforms))
                    $g->setConsole($game->platforms);
                if (isset($game->category))
                    $g->setCategory($game->category);
                if (isset($game->storyline)) {
                    $g->setStoryline($game->storyline);
                } else if (isset($game->summary)) {
                    $g->setStoryline($game->summary);
                } else
                    $g->setStoryline("Description à venir");
                if (isset($game->cover)) {
                    $g->setCover($game->cover->image_id);
                }
                //$firstDate->setTimestamp();
                if (isset($game->first_release_date))
                    $g->setReleaseDate($game->first_release_date);
                array_push($result, $g);
            }
            return $this->render('search_igdb/retour_igdb.html.twig', [
                'games' => $result,
            ]);
        }


        return $this->render('search_igdb/index.html.twig', [
            'formulaire' => $form->createView(),
            'titre' => 'Recherche IGDB! ',
        ]);
    }

    #[Route(path: '/igdb_retour/{id}', name: 'retour_igdb')]
    public function info(
        int $id,
        GenreRepository $repoGenre,
        GameRepository $repoGame,
        EditeurRepository $repoEditeur,
        DeveloppeurRepository $repoDev,
        PegiRepository $repoPegi,
        ConsoleRepository $repoConsole,
        LangageRepository $repoLangue,
        LangagePositionRepository $repoPosition,
        EntityManagerInterface $em
    ): Response {
        //$igdb = new IGDB("z4gd9b08838vy37j5v5u7p3v6st0bm", "4tsa0867uzjizuaqq43w22z2cdbhd1");
        $cle = IGDBUtils::authenticate($this->getParameter('igdb.client'), $this->getParameter('igdb.token'));
        //dd($cle->access_token);
        $igdb = new IGDB($this->getParameter('igdb.client'), $cle->access_token);
        $builder = new IGDBQueryBuilder();
        $query = $builder
            //->search((int) $id)
            ->where("id = " . $id)
            //->where("age_ratings.category = 2")
            ->fields("id, name,first_release_date,category,dlcs.*,expansions.*,expanded_games.*,expansions.*,version_parent, cover.image_id,storyline,slug,genres.name,
            involved_companies.company.name,summary, involved_companies.developer,language_supports.*,
            involved_companies.publisher ,platforms.name,age_ratings.rating,age_ratings.category, websites.*,videos.*, screenshots.*") //,*
            //->limit(3)
            ->build();
        /**
         * @var Game $games
         */
        $games = $igdb->game($query);
        $g  = new GameIGDB();
        $firstDate = new DateTime();
        $gt = new GoogleTranslate('fr'); // Translates into English

        // dd($games);
        // getting the details of the latest query
        foreach ($games as $game) {
            // dd($game);

            //on n'essaie de faire l'insertion que si on ne trouve pas le jeu
            $theGame = $repoGame->findOneBy(['slug' => $game->slug]);

            if ($theGame) {
                $id = $theGame->getId();
                //permet d'afficher un message
                $this->addFlash('info', 'game already created!');
            } else {
                $theGame = new Game;
                $theGame->setTitle($game->name);
                $theGame->setSlug($game->slug);
                $theGame->setIgdbId($game->id);
                //  $theGame->setDescription(isset($game->summary) ? $game->summary : 'Description à venir');
                if (isset($game->storyline)) {

                    $theGame->setDescription($gt->translate($game->storyline));
                } else if (isset($game->summary)) {
                    $theGame->setDescription($gt->translate($game->summary));
                } else
                    $theGame->setDescription("Description à venir");
                if (isset($game->cover)) {
                    $g->setCover($game->cover->image_id);
                }
                //on regarde si l'objet game a bien la notion de first_date_release
                // isset controle aussi si la valeur n'est pas nulle contrairement à property_exists
                // if (property_exists($game, 'first_release_date')) {
                //     $firstDate->setTimestamp($game->first_release_date);
                //     $theGame->setReleaseDate($firstDate);
                // }
                if (isset($game->first_release_date)) {
                    $firstDate->setTimestamp($game->first_release_date);
                    $theGame->setReleaseDate($firstDate);
                }

                //gestion du genre
                if (isset($game->genres)) {
                    foreach ($game->genres as $genre) {
                        $isGenre = $repoGenre->findOneBy(['idIgdb' => $genre->id]);
                        if (isset($isGenre)) {
                            $theGame->addGenre($isGenre);
                        }
                    }
                }
                //website
                if (isset($game->websites)) {
                    foreach ($game->websites as $web) {
                        if ($web->category == "1") {
                            $theGame->setWebsite($web->url);
                        }
                    }
                }

                //pegi
                if (isset($game->age_ratings)) {
                    foreach ($game->age_ratings as $age) {
                        $isPegi = $repoPegi->findOneBy(['id' => $age->rating]);
                        if (isset($isPegi)) {
                            $theGame->setPegi($isPegi);
                        }
                    }
                }
                //console
                if (isset($game->platforms)) {
                    foreach ($game->platforms as $console) {
                        $isConsole = $repoConsole->findOneBy(['idIgdb' => $console->id]);
                        if (isset($isConsole)) {
                            $theGame->addConsole($isConsole);
                        }
                    }
                }
                //gestion des dev et editeurs
                if (isset($game->involved_companies)) {
                    foreach ($game->involved_companies as $involved_companie) {
                        //dd($involved_companie);
                        if ($involved_companie->publisher) {
                            $isEditeur = $repoEditeur->findOneBy(['idIgdbd' => $involved_companie->company->id]);
                            if (!isset($isEditeur)) {
                                $addEditeur = new Editeur;
                                $addEditeur->setName($involved_companie->company->name);
                                $addEditeur->setIdIgdbd($involved_companie->company->id);
                                $em->persist($addEditeur);
                                $em->flush();
                                $theGame->addEditeur($addEditeur);
                            } else {
                                $theGame->addEditeur($isEditeur);
                            }
                        }
                        if ($involved_companie->developer) {
                            $isDeveloppeur = $repoDev->findOneBy(['idIgdbd' => $involved_companie->company->id]);
                            if (!isset($isDeveloppeur)) {
                                $addDev = new Developpeur;
                                $addDev->setName($involved_companie->company->name);
                                $addDev->setIdIgdbd($involved_companie->company->id);
                                $em->persist($addDev);
                                $em->flush();
                                $theGame->addDeveloppeur($addDev);
                            } else {
                                $theGame->addDeveloppeur($isDeveloppeur);
                            }
                        }
                    }
                }
                //            dd($games, $theGame);

                //recopie de l'image
                copy(
                    IGDBUtils::image_url($game->cover->image_id, 'cover_big_2x'),
                    'uploads/covers/' . $game->slug . '-' . $game->cover->image_id . '.png'
                );
                $theGame->setCover($game->slug . '-' . $game->cover->image_id . '.png');
                $em->persist($theGame);
                $em->flush();
                $id = $theGame->getId();

                //gestion des langues
                if (isset($game->language_supports)) {
                    foreach ($game->language_supports as $langue) {
                        $addLangue = new GameLanguage;
                        $addLangue->setJeu($theGame);
                        $addLangue->setLangue($repoLangue->findOneBy(['idIgbd' => $langue->language]));
                        $addLangue->setSupport($repoPosition->findOneBy(['idIgbd' => $langue->language_support_type]));
                        $em->persist($addLangue);
                        $em->flush();
                    }
                }

                //gestion des videos
                if (isset($game->videos)) {
                    foreach ($game->videos as $video) {
                        $addVideo = new Video;
                        $addVideo->setIdYoutube($video->video_id);
                        $addVideo->setTypeVideo($video->name);
                        $addVideo->setJeu($theGame);
                        $em->persist($addVideo);
                        $em->flush();
                    }
                }
                //gestion des screenshots
                if (isset($game->screenshots)) {
                    foreach ($game->screenshots as $screenshot) {
                        //recopie de l'image
                        $return =  copy(
                            IGDBUtils::image_url($screenshot->image_id, 'screenshot_huge_2x'),
                            'uploads/screenshots/' . $game->slug . '-' . $screenshot->image_id . '.png'
                        );
                        if ($return) { //on ne fait l'insersion que si la copie fonctionne
                            $addScreen = new Screenshot;
                            $addScreen->setTitre("Screenshot de " . $game->name);
                            $addScreen->setIgdbId($screenshot->id);
                            $addScreen->setUrl($game->slug . '-' . $screenshot->image_id . '.png');
                            $addScreen->setJeu($theGame);
                            $em->persist($addScreen);
                            $em->flush();
                        }
                    }
                }
                //permet d'afficher un message
                $this->addFlash('success', 'game successfully created!');
            }


            return $this->redirectToRoute('show_game', ['id' => $id]);
            //            dd($games, $theGame);
        }
    }

    // #[Route('/cancel_reservation_coach/{user_id}/{reservation_id}', name: 'cancel_reservation_coach',  methods: ['GET', 'POST'])]
    // #[ParamConverter('user', class: 'App\Entity\User', options: ['mapping' => ['user_id' => 'id']])]
    // #[ParamConverter('reservation', class: 'App\Entity\Reservation', options: ['mapping' => ['reservation_id' => 'id']])]
    #[Route('igdb_pic/{id}/{theGame}', name: 'igdb_pic_maj', methods: ['POST', 'GET'])]
    public function majPic(
        int $id,
        EntityManagerInterface $em,
        Game $theGame,
        ScreenshotRepository $sr,
        VideoRepository $vr,
        LangageRepository $lr,
        LangagePositionRepository $pr,
        GameLanguageRepository $vg,
    ): Response {

        $cle = IGDBUtils::authenticate($this->getParameter('igdb.client'), $this->getParameter('igdb.token'));
        $igdb = new IGDB($this->getParameter('igdb.client'), $cle->access_token);
        $builder = new IGDBQueryBuilder();
        $query = $builder
            ->where("id = " . $id)
            ->fields("id, name,first_release_date, cover.image_id,storyline,slug,genres.name,
             involved_companies.company.name,summary, involved_companies.developer,language_supports.*,
             involved_companies.publisher ,platforms.name,age_ratings.rating,age_ratings.category, websites.*,videos.*, screenshots.*")
            ->build();
        /**
         * @var Game $games
         */
        $games = $igdb->game($query);
        foreach ($games as $game) {
            //gestion des langues
            if (isset($game->language_supports)) {
                foreach ($game->language_supports as $langue) {
                    $language = $lr->findOneBy(['idIgbd' => $langue->language]);
                    $support = $pr->findOneBy(['idIgbd' => $langue->language_support_type]);
                    $isPresentLangue = $vg->findOneBy(['jeu' => $theGame, 'langue' => $language, 'support' => $support]);
                    if (!$isPresentLangue) {
                        $addLangue = new GameLanguage;
                        $addLangue->setJeu($theGame);
                        $addLangue->setLangue($language);
                        $addLangue->setSupport($support);
                        $em->persist($addLangue);
                        $em->flush();
                    }
                }
            }
            //gestion des videos
            if (isset($game->videos)) {
                foreach ($game->videos as $video) {
                    $isPrésentVideo = $vr->findOneBy(['idYoutube' => $video->video_id]);
                    if (!$isPrésentVideo) {
                        $addVideo = new Video;
                        $addVideo->setIdYoutube($video->video_id);
                        $addVideo->setTypeVideo($video->name);
                        $addVideo->setJeu($theGame);
                        $em->persist($addVideo);
                        $em->flush();
                    }
                }
            }
            //gestion des images
            if (isset($game->screenshots)) {
                foreach ($game->screenshots as $screenshot) {
                    //on commence par vérifier que l'on a pas déjà inséré l'iamge
                    $isPrésent = $sr->findOneBy(['igdbId' => $screenshot->id]);
                    if (!$isPrésent) {
                        if (!file_exists('uploads/screenshots/' . $game->slug . '-' . $screenshot->image_id . '.png')) {
                            //recopie de l'image
                            $return =  copy(
                                IGDBUtils::image_url($screenshot->image_id, 'screenshot_huge_2x'),
                                'uploads/screenshots/' . $game->slug . '-' . $screenshot->image_id . '.png'
                            );
                            if ($return) { //on ne fait l'insersion que si la copie fonctionne
                                $addScreen = new Screenshot;
                                $addScreen->setTitre("Screenshot de " . $game->name);
                                $addScreen->setIgdbId($screenshot->id);
                                $addScreen->setUrl($game->slug . '-' . $screenshot->image_id . '.png');
                                $addScreen->setJeu($theGame);
                                $em->persist($addScreen);
                                $em->flush();
                            }
                        }
                    }
                }
            }
        }
        $this->addFlash('success', 'Gallery successfully update!');
        return $this->redirectToRoute('show_game', ['id' => $theGame->getId()]);
    }

    private function getConnection(): IGDBQueryBuilder
    {
        $cle = IGDBUtils::authenticate($this->getParameter('igdb.client'), $this->getParameter('igdb.token'));
        $igdb = new IGDB($this->getParameter('igdb.client'), $cle->access_token);
        $builder = new IGDBQueryBuilder();
        return $builder;
    }
    // $url = "https://api.igdb.com/v4/games";
    // https://id.twitch.tv/oauth2/token?client_id=z4gd9b08838vy37j5v5u7p3v6st0bm&client_secret=xlcm6a9a7z3cxbhp6nc3uhghzng1pr&grant_type=client_credentials
    // $postfields = array(
    //     'Client-ID' => "z4gd9b08838vy37j5v5u7p3v6st0bm",
    //     'Authorization' => "Bearer xlcm6a9a7z3cxbhp6nc3uhghzng1pr",
    //     'Accept' => 'application/json'
    // );
    // $ch = curl_init();
    // curl_setopt($ch, CURLOPT_URL, $url);

    // curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
    // curl_exec($ch);
    // curl_close($ch);
    /*-d "fields age_ratings,aggregated_rating,aggregated_rating_count,alternative_names,artworks,bundles,category,checksum,collection,cover,created_at,dlcs,expanded_games,expansions,external_games,first_release_date,follows,forks,franchise,franchises,game_engines,game_modes,genres,hypes,involved_companies,keywords,multiplayer_modes,name,parent_game,platforms,player_perspectives,ports,rating,rating_count,release_dates,remakes,remasters,screenshots,similar_games,slug,standalone_expansions,status,storyline,summary,tags,themes,total_rating,total_rating_count,updated_at,url,version_parent,version_title,videos,websites;" \
        -H "Client-ID: Client ID" \
        -H "Authorization: Bearer access_token" \
        -H "Accept: application/json";
*/
    // curl -X POST "https://id.twitch.tv/oauth2/token?client_id=z4gd9b08838vy37j5v5u7p3v6st0bm&client_secret=xlcm6a9a7z3cxbhp6nc3uhghzng1pr&grant_type=client_credentials"
    //{"access_token":"4tsa0867uzjizuaqq43w22z2cdbhd1","expires_in":4845539,"token_type":"bearer"}
}
