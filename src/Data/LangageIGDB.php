<?php

namespace App\Data;

use DateTime;
use IGDBUtils;

class LangageIDGB
{
    private $id;
    private $name;
    private $nativeName;
    private $locale;


    public function getID(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getNativeName(): string
    {
        if (isset($this->nativeName)) {
            return $this->nativeName;
        } else {
            return "";
        }
    }
    public function getLocale(): string
    {
        if (isset($this->locale)) {
            return $this->locale;;
        } else {
            return "";
        }
    }
    public function setID(int $id)
    {
        $this->id = $id;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }
    public function setNativeName(string $nativeName)
    {
        $this->nativeName = $nativeName;
    }
    public function setLocale(string $locale)
    {
        $this->locale = $locale;
    }
    /*array:23 [▼
    +"id": 10    +"name": "Racing"
    +"id": 31    +"name": "Adventure"
    +"id": 5     +"name": "Shooter"
    +"id": 35    +"name": "Card & Board Game"
    +"id": 14    +"name": "Sport"


    +"id": 4     +"name": "Fighting"
    +"id": 7     +"name": "Music"
    +"id": 8     +"name": "Platform"
    +"id": 9     +"name": "Puzzle"
    +"id": 11    +"name": "Real Time Strategy (RTS)"
    +"id": 12    +"name": "Role-playing (RPG)"
    +"id": 13    +"name": "Simulator"
    +"id": 15    +"name": "Strategy"
    +"id": 16    +"name": "Turn-based strategy (TBS)"
    +"id": 24    +"name": "Tactical"
    +"id": 26    +"name": "Quiz/Trivia"
    +"id": 25    +"name": "Hack and slash/Beat 'em up"
    +"id": 30    +"name": "Pinball"
    +"id": 33    +"name": "Arcade"
    +"id": 34    +"name": "Visual Novel"
    +"id": 32    +"name": "Indie"
    +"id": 36    +"name": "MOBA"
    +"id": 2     +"name": "Point-and-click"

]*/
}
