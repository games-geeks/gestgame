<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201219084423 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Rendre unique certains champs';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE UNIQUE INDEX UNIQ_393ADA7D5EC92553 ON consoles (long_name)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_CC8D6F55E237E06 ON constructeurs (name)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FF232B312B36786B ON games (title)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_393ADA7D5EC92553 ON consoles');
        $this->addSql('DROP INDEX UNIQ_CC8D6F55E237E06 ON constructeurs');
        $this->addSql('DROP INDEX UNIQ_FF232B312B36786B ON games');
    }
}
