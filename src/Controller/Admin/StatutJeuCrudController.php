<?php

namespace App\Controller\Admin;

use App\Entity\StatutJeu;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class StatutJeuCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return StatutJeu::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
