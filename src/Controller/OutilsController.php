<?php

namespace App\Controller;

use IGDB;
use IGDBUtils;
use IGDBQueryBuilder;
use App\Form\SearchIGDBType;
use App\Data\CompagnieIDGB;
use App\Data\ConsoleIDGB;
use App\Data\LangageIDGB;
use App\Data\LangageITypeGDB;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class OutilsController extends AbstractController
{
    #[Route(path: '/outils_igdb', name: 'outils_recherche_companie', methods: ['GET', 'POST'])]
    public function index(Request $request): Response
    {
        $form = $this->createForm(SearchIGDBType::class, null, [
            'search_controller' => 'outils',
            'search_label' => 'Entrer votre recherche'
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $cle = IGDBUtils::authenticate($this->getParameter('igdb.client'), $this->getParameter('igdb.token'));
            //dd($cle->access_token);
            $igdb = new IGDB($this->getParameter('igdb.client'), $cle->access_token);
            $builder = new IGDBQueryBuilder();

            if ($form->get('in')->getData() == "company") {
                return $this->render('outils/searchCompnie.html.twig', [
                    'companies' => $this->searchCompany($builder, $igdb, $form->get('search')->getData()),
                ]);
            }

            if ($form->get('in')->getData() == "language") {
                return $this->render('outils/searchCompnie.html.twig', [
                    'language' => $this->searchLanguage($builder, $igdb, $form->get('search')->getData()),
                ]);
            }

            if ($form->get('in')->getData() == "languageType") {
                return $this->render('outils/searchCompnie.html.twig', [
                    'language' => $this->searchLanguageSupportType($builder, $igdb, $form->get('search')->getData()),
                ]);
            }

            if ($form->get('in')->getData() == "console") {
                //$this->searchConsole($builder, $igdb, $form->get('search')->getData());
                return $this->render('outils/searchConsole.html.twig', [
                    'values' => $this->searchConsole($builder, $igdb, $form->get('search')->getData()),
                ]);
            }
        }



        return $this->render('outils/index.html.twig', [
            'formulaire' => $form->createView(),
        ]);
    }

    private function searchCompany(IGDBQueryBuilder $builder, IGDB $igdb, String $search)
    {
        $result = array();
        $offset = 1;

        //dd($form->get('search')->getData());
        $query =
            $builder
            ->fields("id, name,developed,published")
            ->build();
        $nbCompagnies = $igdb->company($query, true);

        while ($offset < $nbCompagnies->count) {
            $query =
                //->search((int) $id)
                $builder
                ->fields("id, name,developed,published")
                ->limit(500)
                ->offset($offset)
                ->build();
            $compagnies = $igdb->company($query);


            foreach ($compagnies as $compagnie) {
                $g  = new CompagnieIDGB();
                $g->setID($compagnie->id);
                $g->setName($compagnie->name);
                $g->setDeveloper((isset($compagnie->developed) ? true : false));
                $g->setPublisher((isset($compagnie->published) ? true : false));
                //if (!(stripos($compagnie->name, $form->get('search')->getData()) === false)) {
                if (!(stripos($compagnie->name, $search) === false)) {
                    array_push($result, $g);
                }
            }
            $offset += 500;
        }
        return $result;
    }
    private function searchConsole(IGDBQueryBuilder $builder, IGDB $igdb, String $search)
    {
        $result = array();


        //dd($form->get('search')->getData());
        $query =
            $builder
            ->fields("*")
            ->build();
        $nbValue = $igdb->platform($query, true);

        $query =
            //->search((int) $id)
            $builder
            ->fields("id, name, alternative_name, abbreviation")
            ->limit($nbValue->count)
            //->offset($offset)
            ->build();
        $values = $igdb->platform($query);
        //dd($values);

        foreach ($values as $value) {
            $g  = new ConsoleIDGB();
            $g->setID($value->id);
            $g->setName($value->name);
            if (isset($value->abbreviation))
                $g->setAbbreviation($value->abbreviation);
            if (isset($value->alternative_name))
                $g->setAlternativeName($value->alternative_name);

            //if (!(stripos($compagnie->name, $form->get('search')->getData()) === false)) {
            if (!(stripos($value->name, $search) === false)) {
                array_push($result, $g);
            }
        }
        return $result;
    }

    private function searchLanguage(IGDBQueryBuilder $builder, IGDB $igdb)
    {
        $result = array();


        //dd($form->get('search')->getData());
        $query =
            $builder
            ->fields("*")
            ->build();
        $nbValue = $igdb->language($query, true);

        $query =
            //->search((int) $id)
            $builder
            ->fields("id, locale,name,native_name")
            ->limit($nbValue->count)
            //->offset($offset)
            ->build();
        $values = $igdb->language($query);
        dd($values);

        foreach ($values as $value) {
            $g  = new LangageIDGB();
            $g->setID($value->id);
            $g->setName($value->name);
            if (isset($value->locale))
                $g->setLocale($value->locale);
            if (isset($value->native_name))
                $g->setNativeName($value->native_name);

            //if (!(stripos($compagnie->name, $form->get('search')->getData()) === false)) {
            // if (!(stripos($value->name, $search) === false)) {
            //     array_push($result, $g);
            // }
        }
        return $result;
    }

    private function searchLanguageSupportType(IGDBQueryBuilder $builder, IGDB $igdb)
    {
        $result = array();


        //dd($form->get('search')->getData());
        $query =
            $builder
            ->fields("*")
            ->build();
        $nbValue = $igdb->language_support_types($query, true);

        $query =
            //->search((int) $id)
            $builder
            ->fields("id, name")
            ->limit($nbValue->count)
            //->offset($offset)
            ->build();
        $values = $igdb->language_support_types($query);
        dd($values);

        foreach ($values as $value) {
            $g  = new LangageITypeGDB();
            $g->setID($value->id);
            $g->setName($value->name);


            //if (!(stripos($compagnie->name, $form->get('search')->getData()) === false)) {
            // if (!(stripos($value->name, $search) === false)) {
            //     array_push($result, $g);
            // }
        }
        return $result;
    }
}
