<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\Timestampable;
use App\Repository\GameUserRepository;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;

#[ORM\Table(name: 'game_users')]
#[ORM\Entity(repositoryClass: GameUserRepository::class)]
#[ORM\HasLifecycleCallbacks]
class GameUser
{
    use Timestampable;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: Game::class, inversedBy: 'gameUsers')]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\NotBlank(message: 'Le jeu ne peut être vide')]
    private $game;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'gameUsers')]
    #[ORM\JoinColumn(nullable: false)]
    private $user;

    #[ORM\Column(type: 'text', nullable: true)]
    private $remarque;

    #[ORM\Column(type: 'float', nullable: true)]
    private $price;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $purchaseDate;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank(message: 'Le titre ne peut être vide')]
    private $name;

    #[ORM\ManyToOne(targetEntity: Console::class, inversedBy: 'gameUsers')]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\NotBlank(message: 'Le support ne peut être vide')]
    private $consoles;

    #[ORM\Column(type: 'boolean', nullable: true, options: ['default' => 0])]
    private $isDigital;

    #[ORM\Column(type: 'boolean', nullable: true, options: ['default' => 0])]
    private $haveSteelbook;

    #[ORM\Column(type: 'boolean', nullable: true, options: ['default' => 0])]
    private $likes;

    #[ORM\Column(type: 'boolean', nullable: true, options: ['default' => 0])]
    private $wishes;

    #[ORM\ManyToOne(inversedBy: 'gameUsers')]
    private ?StatutJeu $statut = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGame(): ?Game
    {
        return $this->game;
    }

    public function setGame(?Game $game): self
    {
        $this->game = $game;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getRemarque(): ?string
    {
        return $this->remarque;
    }

    public function setRemarque(?string $remarque): self
    {
        $this->remarque = $remarque;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPurchaseDate(): ?\DateTimeInterface
    {
        return $this->purchaseDate;
    }

    public function setPurchaseDate(?\DateTimeInterface $purchaseDate): self
    {
        $this->purchaseDate = $purchaseDate;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getConsoles(): ?console
    {
        return $this->consoles;
    }

    public function setConsoles(?console $consoles): self
    {
        $this->consoles = $consoles;

        return $this;
    }

    public function getIsDigital(): ?bool
    {
        return $this->isDigital;
    }

    public function setIsDigital(?bool $isDigital): self
    {
        $this->isDigital = $isDigital;

        return $this;
    }

    public function getHaveSteelbook(): ?bool
    {
        return $this->haveSteelbook;
    }

    public function setHaveSteelbook(?bool $haveSteelbook): self
    {
        $this->haveSteelbook = $haveSteelbook;

        return $this;
    }

    public function getLikes(): ?bool
    {
        return $this->likes;
    }

    public function setLikes(bool $likes): self
    {
        $this->likes = $likes;

        return $this;
    }

    public function getWishes(): ?bool
    {
        return $this->wishes;
    }

    public function setWishes(bool $wishes): self
    {
        $this->wishes = $wishes;

        return $this;
    }

    public function getStatut(): ?StatutJeu
    {
        return $this->statut;
    }

    public function setStatut(?StatutJeu $statut): static
    {
        $this->statut = $statut;

        return $this;
    }
}
