<?php

declare(strict_types=1);

namespace App\Service;

use IGDB;
use IGDBUtils;
use IGDBQueryBuilder;
use App\Entity\Langage;
use App\Entity\LangagePosition;
use App\Repository\LangagePositionRepository;
use App\Repository\LangageRepository;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class OutilIgdbService
{
    public function __construct(
        private readonly LangageRepository $repoLangage,
        private readonly LangagePositionRepository $langagePositionRepository,
        private readonly ParameterBagInterface $getParams

    ) {
    }

    public function createLanguageFromIGDB(): void
    {
        $cle = IGDBUtils::authenticate($this->getParams->get('igdb.client'), $this->getParams->get('igdb.token'));
        $igdb = new IGDB($this->getParams->get('igdb.client'), $cle->access_token);

        $builder = new IGDBQueryBuilder();
        $query =
            $builder
            ->fields("*")
            ->build();
        $nbValue = $igdb->language($query, true);

        $query =
            $builder
            ->fields("id, locale,name,native_name")
            ->limit($nbValue->count)
            ->build();
        $languages = $igdb->language($query);


        foreach ($languages as $lng) {
            $langage = new Langage;
            $isPresent = $this->repoLangage->findOneBy(['idIgbd' => $lng->id]);
            if (!isset($isPresent)) {
                $langage->setIdIgbd($lng->id);
                $langage->setName($lng->name);
                $langage->setLocale($lng->locale);
                $langage->setNativeName($lng->native_name);
                $this->repoLangage->add($langage, true);
            }
        }
    }

    public function createLanguagePositionFromIGDB(): void
    {
        $cle = IGDBUtils::authenticate($this->getParams->get('igdb.client'), $this->getParams->get('igdb.token'));
        $igdb = new IGDB($this->getParams->get('igdb.client'), $cle->access_token);

        $builder = new IGDBQueryBuilder();
        $query =
            $builder
            ->fields("*")
            ->build();
        $nbValue = $igdb->language_support_types($query, true);

        $query =
            $builder
            ->fields("id, name")
            ->limit($nbValue->count)
            ->build();
        $languages = $igdb->language_support_types($query);


        foreach ($languages as $lng) {
            $langage = new LangagePosition;
            $isPresent = $this->langagePositionRepository->findOneBy(['idIgbd' => $lng->id]);
            if (!isset($isPresent)) {
                $langage->setIdIgbd($lng->id);
                $langage->setName($lng->name);
                $this->langagePositionRepository->add($langage, true);
            }
        }
    }
}
