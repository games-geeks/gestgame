<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\Timestampable;
use App\Repository\LangagePositionRepository;

#[ORM\Entity(repositoryClass: LangagePositionRepository::class)]
#[ORM\HasLifecycleCallbacks]
class LangagePosition
{

    use Timestampable;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $idIgbd;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'support', targetEntity: GameLanguage::class)]
    private Collection $gameLanguages;

    public function __construct()
    {
        $this->gameLanguages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function getIdIgbd(): ?int
    {
        return $this->idIgbd;
    }

    public function setIdIgbd(?int $idIgdbd): self
    {
        $this->idIgbd = $idIgdbd;
        return $this;
    }

    /**
     * @return Collection<int, GameLanguage>
     */
    public function getGameLanguages(): Collection
    {
        return $this->gameLanguages;
    }

    public function addGameLanguage(GameLanguage $gameLanguage): static
    {
        if (!$this->gameLanguages->contains($gameLanguage)) {
            $this->gameLanguages->add($gameLanguage);
            $gameLanguage->setSupport($this);
        }

        return $this;
    }

    public function removeGameLanguage(GameLanguage $gameLanguage): static
    {
        if ($this->gameLanguages->removeElement($gameLanguage)) {
            // set the owning side to null (unless already changed)
            if ($gameLanguage->getSupport() === $this) {
                $gameLanguage->setSupport(null);
            }
        }

        return $this;
    }
}
