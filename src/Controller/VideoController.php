<?php

namespace App\Controller;


use App\Entity\Game;
use App\Entity\Video;
use App\Form\VideoType;
use App\Repository\VideoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;



class VideoController extends AbstractController
{
    #[Route(path: '/video', name: 'video_index', methods: ['GET'])]
    public function index(VideoRepository $videoRepository): Response
    {
        return $this->render('video/index.html.twig', [
            'videos' => $videoRepository->findAll(),
        ]);
    }
    #[Route(path: '/video/create', name: 'create_video', methods: ['GET', 'POST'])]
    #[Security("is_granted('GAME_MANAGE')")]
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        if (!$this->getUser()) {
            $this->addFlash('error', 'Not Connected!');
            return $this->redirectToRoute('app_login');
        }

        $video = new Video;
        if ($request->query->get('id')) {
            $repo = $em->getRepository(Game::class);
            $refGame = $repo->findOneBy(['id' => $request->query->get('id')]);
            $video->setJeu($refGame);
        }

        $form = $this->createForm(VideoType::class, $video);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($video);
            $em->flush();
            $id = $video->getJeu()->getId();
            //permet d'afficher un message
            $this->addFlash('success', 'video successfully added!');
            return $this->redirectToRoute('show_game', ['id' => $id]);
        }
        return $this->render('video/create.html.twig', [
            'controller_name' => 'GameController',
            'monForm' => $form->createView(),
        ]);
    }


    #[Route(path: '/video/{id}', name: 'show_video', methods: ['GET'])]
    #[Security("is_granted('GAME_MANAGE')")]
    public function show(Video $video): Response
    {
        return $this->render('vi/show.html.twig', [
            'video' => $video,
        ]);
    }
    #[Route(path: '/video/{id}/edit', name: 'edit_video', methods: ['GET', 'POST'])]
    #[Security("is_granted('GAME_MANAGE')")]
    public function edit(Request $request, Video $video, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(Video1Type::class, $video);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('vi_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('vi/edit.html.twig', [
            'video' => $video,
            'form' => $form,
        ]);
    }
    #[Route(path: '/video/{id}', name: 'delete_video', methods: ['POST'])]
    #[Security("is_granted('GAME_MANAGE')")]
    public function delete(Request $request, Video $video, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete' . $video->getId(), $request->request->get('_token'))) {
            $entityManager->remove($video);
            $entityManager->flush();
        }

        return $this->redirectToRoute('vi_index', [], Response::HTTP_SEE_OTHER);
    }
}
