<?php

namespace App\Repository;

use App\Entity\User;
use App\Data\SearchData;
use App\Entity\GameUser;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method GameUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method GameUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method GameUser[]    findAll()
 * @method GameUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GameUserRepository extends ServiceEntityRepository
{
    /**
     * @var PaginatorInterface
     */
    private $paginator;
    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, GameUser::class);
        $this->paginator = $paginator;
    }


    /**
     *
     * @return GameUser[] Returns an array of GameUser objects filtered by User
     */
    public function findByUser($value): array
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.user = :val')
            ->setParameter('val', $value)
            ->orderBy('g.name')
            ->getQuery()
            ->getResult();
    }


    // /**
    //  * @return GameUser[] Returns an array of GameUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GameUser
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    /**
     * Récupère les jeux en lien avec une recherche
     * @return PaginationInterface
     */
    public function findSearch(SearchData $search, User $user, int $nbReturn = 48): PaginationInterface
    {
        $query = $this->getSearchQuery($search, $user)->getQuery();


        $return = $this->paginator->paginate(
            $query,
            $search->getPage(),
            $nbReturn
        );
        $return->setCustomParameters([
            'align' => 'center', # center|right (for template: twitter_bootstrap_v4_pagination)
            'size' => 'small', # small|large (for template: twitter_bootstrap_v4_pagination)
            'style' => 'bottom',
            'span_class' => 'whatever',
        ]);
        return $return;
    }


    private function getSearchQuery(SearchData $search, User $user, $ignoreWish = false): QueryBuilder
    {
        $query = $this
            ->createQueryBuilder('u')
            ->select('u', 'g', 'p', 'c', 's')
            ->leftjoin('u.game', 'g')
            ->leftjoin('g.pegi', 'p')
            ->leftjoin('u.consoles', 'c')
            ->leftjoin('u.statut', 's')
            ->orderBy('u.id', 'DESC');

        $query = $query
            ->andWhere('u.user = :val')
            ->setParameter('val', $user);

        if (!empty($search->qSearch)) {
            $query = $query
                ->andWhere('u.name LIKE :qSearch')
                ->setParameter('qSearch', "%{$search->qSearch}%");
        }

        if (!empty($search->statutJeux)) {
            $query = $query
                ->andWhere('s.id IN (:statut)')
                ->setParameter('statut', $search->statutJeux);
            // dd($query);
        }

        if (!empty($search->consoles)) {
            $query = $query
                ->andWhere('c.id IN (:consoles)')
                ->setParameter('consoles', $search->consoles);
        }

        if (!empty($search->isDemat)) {
            $query =  $query
                ->andWhere('u.isDigital=1');
        }

        if (!empty($search->isWish)) {
            $query =  $query
                ->andWhere('u.wishes=1');
        }
        if (!empty($search->isLike)) {
            $query =  $query
                ->andWhere('u.likes=1');
        }
        if (!empty($search->pegis)) {
            $query = $query
                ->andWhere('p.id IN (:pegis)')
                ->setParameter('pegis', $search->pegis);
        }

        return $query;
    }
}
//select date_format(gu.purchase_date , '%Y%m') as month, count(*), sum (gu.price )  from game_users gu group by month order by month;