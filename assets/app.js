/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

import './bootstrap.js';
import 'bootstrap/dist/js/bootstrap.bundle.js';
// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';
import './styles/ckeditor5.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-free/css/all.css';
import 'tom-select/dist/css/tom-select.bootstrap5.css';
// import bsCustomFileInput from 'bs-custom-file-input';

// start the Stimulus application
// bsCustomFileInput.init();

//ajout des tooltips
const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))
// var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
// var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
//     return new bootstrap.Tooltip(tooltipTriggerEl)
// })

