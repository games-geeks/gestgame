<?php

namespace App\Controller;

use App\Entity\GameLanguage;
use App\Form\GameLanguageType;
use App\Repository\GameLanguageRepository;
use App\Repository\GameRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('language', name: 'language')]
class GameLanguageController extends AbstractController
{
    #[Route('/', name: '_index')]
    public function index(): Response
    {
        return $this->render('game_language/index.html.twig', [
            'controller_name' => 'GameLanguageController',
        ]);
    }

    #[Route('/add', name: '_add')]
    #[IsGranted('ROLE_ADMIN')]
    /** or use Security attribute     #[Security("is_granted('ROLE_ADMIN') and is_granted('ROLE_FRIENDLY_USER')")]*/
    public function add(Request $request, GameLanguageRepository $gameLanguageRepository, GameRepository $gameRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $gl = new GameLanguage();

        //on récupéère l'id du jeu
        if ($request->query->get('id')) {
            $refGame = $gameRepository->findOneBy(['id' => $request->query->get('id')]);
            $gl->setJeu($refGame);
        }

        $form = $this->createForm(GameLanguageType::class, $gl);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $gameLanguageRepository->add($gl, true);
            return $this->redirectToRoute('show_game', ['id' => $request->query->get('id')]);
        }

        return $this->render('game_language/new.html.twig', [
            'gl' => $gl,
            'monForm' => $form,
        ]);
    }
}
